<?php

function getNodeData($listOfNodes, $startdate, $enddate)
{
    
    try
    {
        $m = new MongoClient('mongodb://localhost');
        $db = $m->root;
    }
    catch (Exception $e) 
    {
        echo $e->getMessage();
    }
    
    $prevmongotime = new MongoDate($startdate);
    $startmongotime = new MongoDate($enddate);
    $collection = new MongoCollection($db, 'data');
    $data["Sensor"] = "T";
    foreach($listOfNodes as $Node)
    {
        $filter = array('NodeID'=>$Node,'Time'=>array('$gte'=>$prevmongotime,'$lte'=>$startmongotime));
        $database = $collection->find($filter);
        $nodeBattery = array();
        $nodeSoilTemp = array();
        $nodeAirTemp = array();
        $nodeHumidity = array();
        $nodeMoisture = array();
        $nodeTimes = array();
        foreach($database as $oneNodeData)
        {
            array_push($nodeBattery,$oneNodeData["Values"]["Voltage"]*100/5.084);
            array_push($nodeSoilTemp,$oneNodeData["Values"]["Ground Temp"]);
            array_push($nodeAirTemp,$oneNodeData["Values"]["Air Temp"]);
            array_push($nodeHumidity,$oneNodeData["Values"]["Humidity"]);
            array_push($nodeMoisture,$oneNodeData["Values"]["Moisture"]);
            array_push($nodeTimes,$oneNodeData["Time"]->sec*1000);
        }
        
        $nodeNameDB = new MongoCollection($db, 'nodes');
        $filter = array('NodeID'=>$Node);
        $nodeName = $nodeNameDB->findOne($filter);
        $nodeName = $nodeName['Name'];
        
        //Moisture
        $data["Data"]["Moisture"]["Sensor"][$Node]["Name"] = $nodeName;   
        $data["Data"]["Moisture"]["Sensor"][$Node]["Time"] = $nodeTimes;
        $nodeMoisture = array_map( function($val) { return round($val / 10.24,2); }, $nodeMoisture);        
        $data["Data"]["Moisture"]["Sensor"][$Node]["Value"] = $nodeMoisture;
        $data["Data"]["Moisture"]["Sensor"][$Node]["Units"] = array("Unit" => "%", "Min" => 0, "Max" => 100);        
        $data["Data"]["Moisture"]["Sensor"][$Node]["MaxVal"] = max($nodeMoisture);
        $data["Data"]["Moisture"]["Sensor"][$Node]["MinVal"] = min($nodeMoisture);
        $data["Data"]["Moisture"]["Sensor"][$Node]["DisplayType"] = "Range-Map";      
        $data["Data"]["Moisture"]["Sensor"][$Node]["Group"] = "Moisture";
        
        //Air Temp
        $data["Data"]["Air Temperature"]["Sensor"][$Node]["Name"] = $nodeName;    
        $data["Data"]["Air Temperature"]["Sensor"][$Node]["Time"] = $nodeTimes;        
        $data["Data"]["Air Temperature"]["Sensor"][$Node]["Value"] = $nodeAirTemp;
        $data["Data"]["Air Temperature"]["Sensor"][$Node]["Units"] = array("Unit" => "&deg;F", "Min" => 0, "Max" => 120);        
        $data["Data"]["Air Temperature"]["Sensor"][$Node]["MaxVal"] = max($nodeAirTemp);
        $data["Data"]["Air Temperature"]["Sensor"][$Node]["MinVal"] = min($nodeAirTemp);
        $data["Data"]["Air Temperature"]["Sensor"][$Node]["DisplayType"] = "Range-Map";
        $data["Data"]["Air Temperature"]["Sensor"][$Node]["Group"] = "Air Temperature";
        
        //Soil Temp
        $data["Data"]["Soil Temperature"]["Sensor"][$Node]["Name"] = $nodeName;   
        $data["Data"]["Soil Temperature"]["Sensor"][$Node]["Time"] = $nodeTimes;        
        $data["Data"]["Soil Temperature"]["Sensor"][$Node]["Value"] = $nodeSoilTemp;
        $data["Data"]["Soil Temperature"]["Sensor"][$Node]["Units"] = array("Unit" => "&deg;F", "Min" => 0, "Max" => 120);        
        $data["Data"]["Soil Temperature"]["Sensor"][$Node]["MaxVal"] = max($nodeSoilTemp);
        $data["Data"]["Soil Temperature"]["Sensor"][$Node]["MinVal"] = min($nodeSoilTemp);
        $data["Data"]["Soil Temperature"]["Sensor"][$Node]["DisplayType"] = "Range-Map";        
        $data["Data"]["Soil Temperature"]["Sensor"][$Node]["Group"] = "Soil Temperature";
        
        //Humidity
        $data["Data"]["Relative Humidity"]["Sensor"][$Node]["Name"] = $nodeName;   
        $data["Data"]["Relative Humidity"]["Sensor"][$Node]["Time"] = $nodeTimes;        
        $data["Data"]["Relative Humidity"]["Sensor"][$Node]["Value"] = $nodeHumidity;
        $data["Data"]["Relative Humidity"]["Sensor"][$Node]["Units"] = array("Unit" => "%", "Min" => 0, "Max" => 100);        
        $data["Data"]["Relative Humidity"]["Sensor"][$Node]["MaxVal"] = max($nodeHumidity);
        $data["Data"]["Relative Humidity"]["Sensor"][$Node]["MinVal"] = min($nodeHumidity);
        $data["Data"]["Relative Humidity"]["Sensor"][$Node]["DisplayType"] = "Range-Map";        
        $data["Data"]["Relative Humidity"]["Sensor"][$Node]["Group"] = "Humidity";
        
        //Battery
        $data["Data"]["Battery Life"]["Sensor"][$Node]["Name"] = $nodeName;    
        $data["Data"]["Battery Life"]["Sensor"][$Node]["Time"] = $nodeTimes;        
        $data["Data"]["Battery Life"]["Sensor"][$Node]["Value"] = $nodeBattery;
        $data["Data"]["Battery Life"]["Sensor"][$Node]["Units"] = array("Unit" => "%", "Min" => 0, "Max" => 100);        
        $data["Data"]["Battery Life"]["Sensor"][$Node]["MaxVal"] = max($nodeBattery);
        $data["Data"]["Battery Life"]["Sensor"][$Node]["MinVal"] = min($nodeBattery);
        $data["Data"]["Battery Life"]["Sensor"][$Node]["DisplayType"] = "Range-Map";
        $data["Data"]["Battery Life"]["Sensor"][$Node]["Group"] = "Battery";
    }
    
    return $data;
}

?>
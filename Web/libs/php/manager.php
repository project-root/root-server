<?php

$Mode = $_GET['Mode'];
session_start();
try
{
    $m = new MongoClient('mongodb://localhost');
    $db = $m->root;
}
catch (Exception $e) 
{
    echo $e->getMessage();
}

switch($Mode){

    case 'GetIPInfo':
        $addr = $_SERVER['REMOTE_ADDR'];
		$addr_bin = inet_pton($addr);
		// Check prefix
		if( substr($addr_bin, 0, strlen($v4mapped_prefix_bin)) == $v4mapped_prefix_bin) {
		  // Strip prefix
		  $addr_bin = substr($addr_bin, strlen($v4mapped_prefix_bin));
		}

		// Convert back to printable address in canonical form
		$ip = inet_ntop($addr_bin);
		$details = json_decode(file_get_contents("http://ipinfo.io/".$ip."/json"));
		$Country =  $details->country;
		$Region =  $details->region;
		$City = $details->city;
        $Postal = $details->postal;
        
        echo json_encode(array("Country"=>$Country,"Region"=>$Region,"City"=>$City,"Postal"=>$Postal));
        
    break;
    
    case 'GetCrops':
        $collection = new MongoCollection($db, 'crops');
        $data = $collection->find(array(),array("Crop Name"=>1,"_id"=>0));
        
        $out = array();
        
        foreach ($data as $row)
            array_push($out,$row["Crop Name"]);
            
        echo json_encode($out); 
        
    break;

    case 'CreateNewUser':

        if (isset($_GET['Name']))
		$Name= $_GET['Name'];
        else
            $Name = "";
        if (isset($_GET['Email']))
            $Email= strtolower(addslashes(($_GET['Email'])));
        else
            $Email = "";
        if (isset($_GET['Password1']))
            $PWD= addslashes($_GET['Password1']);
        else
            $PWD = "";
        if (isset($_GET['Password2']))
            $PWD2= addslashes($_GET['Password2']);
        else
            $PWD2 = "";
        if (isset($_GET['Country']))
            $Country= $_GET['Country'];
        else
            $Country = "";
        if (isset($_GET['State']))
            $State= $_GET['State'];
        else
            $State = "";
        if (isset($_GET['City']))
            $City= $_GET['City'];
        else
            $City = "";
        if (isset($_GET['Zip']))
            $Zip= $_GET['Zip'];
        else
            $Zip = "";
        if (isset($_GET['Acreage']))
            $Acerage= $_GET['Acreage'];
        else
            $Acerage = "";
        if (isset($_GET['Crop']))
            $Crop= $_GET['Crop'];
        else
            $Crop = "";    
        if (isset($_GET['SoilType']))
            $SoilType= $_GET['SoilType'];
        else
            $SoilType = "";
        if (isset($_GET['IrrDays']))
            $IrrDays= $_GET['IrrDays'];
        else
            $IrrDays = "";
        if (isset($_GET['IrrTime']))
            $IrrTime= $_GET['IrrTime'];
        else
            $IrrTime = "";
        
        $collection = new MongoCollection($db, 'users');
        $filter = array('Email'=>$Email);
        $cursor = ($collection->find($filter));
        
        //check if there is already an account with that email
        if($cursor->count() > 0)
        {
                echo json_encode("Email");
                return;
        }
        
        if($PWD == $PWD2 && filter_var($Email, FILTER_VALIDATE_EMAIL) && $PWD2 != "" && $Email != "" && $Name != "")
        {
            $UserID = uniqid (rand (),true);
            $PWD = addslashes($PWD);
            include 'passwordhash.php';
            $hasher = new PasswordHash(10, FALSE);
            $salt = $hasher->get_salt(21);
            $hashed_password = $hasher->HashPassword($PWD.$UserID.$salt);
            
            $newUser = array();
            $newUser['UserID'] = $UserID;
            $newUser['Email'] = $Email;
            $newUser['Name'] = $Name;
            $newUser['Salt'] = $salt;
            $newUser['Password'] = $hashed_password;
            $newUser['Country'] = $Country;
            $newUser['State'] = $State;
            $newUser['City'] = $City;
            $newUser['Address'] = "";
            $newUser['Phone'] = "";
            $newUser['Irrigation System'] = "";
            $newUser['Water Source'] = "";
            $newUser['Acreage'] = $Acerage;
            $newUser['Zip'] = $Zip;
            $newUser['Signup Date'] = new MongoDate();
            $newUser['Last Date'] = new MongoDate();
            $newUser['Visits'] = 1;
            $newUser['Has Mobile'] = "F";
            $newUser['Has Sensors'] = "F";
            $newUser['Confirmed'] = "F";
            $newUser['Soil Type'] = "";
            $newUser['Zones'] = array(); 
            
            $obj_id = $db->users->insert($newUser);

            $message = '<html><body>Thank you for signing up!<br><br>To confirm your account, please click <a href="http://'.$_SERVER['HTTP_HOST'].'/app/confirmemail.html?A='.$_SESSION['User']['UserID'].'&B='.$_SESSION['User']['Salt'].'">here</a>.<br><br>You can also copy and paste the following link into your browser:<br>http://'.$_SERVER['HTTP_HOST'].'/app/confirmemail.html?A='.$_SESSION['User']['UserID'].'&B='.$_SESSION['User']['Salt'].'<br><br>Remember to share '.$_SERVER['HTTP_HOST'].' with your friends!<br><br>Regards,<br>rooT team</body></html>';
            $to      =  $Email;
            $subject = 'Welcome from rooT';
            $headers = 'From: rooT <info@project-root.com>' . "\r\n" .
                        'Reply-To: rooT <info@project-root.com>' . "\r\n" .
                        'MIME-Version: 1.0' . "\r\n" .
                        'Content-Type: text/html; charset=ISO-8859-1';

            mail($to, $subject, $message, $headers);
            
            //log user in
            $out = ($collection->findOne($filter));
            $_SESSION['User'] = $out;
            
            echo json_encode("Success");
        }
        else
            echo json_encode("Invalid");
    break;
    
    case 'ResendEmail':
        
        $out = "Fail";
        
        if(isset($_SESSION['User']))
        {
            $message = '<html><body>Thank you for signing up!<br><br>To confirm your account, please click <a href="http://'.$_SERVER['HTTP_HOST'].'/app/confirmemail.html?A='.$_SESSION['User']['UserID'].'&B='.$_SESSION['User']['Salt'].'">here</a>.<br><br>You can also copy and paste the following link into your browser:<br>http://'.$_SERVER['HTTP_HOST'].'/app/confirmemail.html?A='.$_SESSION['User']['UserID'].'&B='.$_SESSION['User']['Salt'].'<br><br>Remember to share '.$_SERVER['HTTP_HOST'].' with your friends!<br><br>Regards,<br>rooT team</body></html>';
            $to      =  $_SESSION['User']['Email'];
            $subject = 'Welcome from rooT';
            $headers = 'From: rooT <info@project-root.com>' . "\r\n" .
                        'Reply-To: rooT <info@project-root.com>' . "\r\n" .
                        'MIME-Version: 1.0' . "\r\n" .
                        'Content-Type: text/html; charset=ISO-8859-1';

            mail($to, $subject, $message, $headers);
            $out = "Success";
        }
        
        echo json_encode($out);
    
    break;
    
    case 'UpdateUserDetails':

        $User = $_SESSION['User'];
        $User['Name'] = $_GET['Name'];
        $User['Country'] = $_GET['Country'];
        $User['State'] = $_GET['State'];
        $User['City'] = $_GET['City'];
        $User['Address'] = $_GET['Address'];
        $User['Phone'] = $_GET['Phone'];
        $User['Irrigation System'] = $_GET['IrrSystem'];
        $User['Water Source'] = $_GET['WaterSource'];
        $User['Acreage'] = $_GET['Acreage'];
        $User['Zip'] = $_GET['Zip'];
        $User['Soil Type'] = $_GET['SoilType'];

        $out = "Fail";
        
        if(isset($_SESSION['User']))
        {
            $filter = array("UserID"=>$_SESSION['User']['UserID']);
            
            $collection = new MongoCollection($db, 'users');
            $collection->update($filter,$User);
            
            //update user
            $updated = ($collection->findOne($filter));
            $_SESSION['User'] = $updated;
            
            $out = "Success";
        }
        
        echo json_encode($out);
    
    break;
    
    case 'GetUserDetails':
        $out = "Fail";
        if(!isset($_SESSION['User']))
        {
            $Email = strtolower(addslashes(($_GET['Email'])));
            $Password = addslashes(($_GET['Password']));
            
            $collection = new MongoCollection($db, 'users');
            
            $filter = array('Email'=>$Email);
            
            $out = ($collection->findOne($filter));
            
            include 'passwordhash.php';
            $hasher = new PasswordHash(10, FALSE);
            $PWD_check = $hasher->CheckPassword($Password.$out['UserID'].$out['Salt'], $out['Password']);
            
            if($PWD_check)
            {
                $_SESSION['User'] = $out;
                //Update Vistits and Last Visit Date
                $collection->update($filter,array('$set'=>array('Last Date' => new MongoDate()),'$set'=>array('Visits' => intval($_SESSION['User']['Visits']) + 1)));
                
            }
            else
            {
                $_SESSION['User'] = null;
                $out = "Fail";
            }    
        }
        else
        {
            $collection = new MongoCollection($db, 'users');
            $filter = array('Email'=>$_SESSION['User']['Email']);
            $out = ($collection->findOne($filter));
            $_SESSION['User'] = $out;
        }
        
        if(is_array($out))
        {
            unset($out['Password']);
            unset($out['Salt']);
            unset($out['UserID']);
        }
        echo json_encode($out);
          
    break;
    
    case "ZipToLatLon":
        $Zip = $_GET["Zip"];
        $response = new SimpleXMLElement(file_get_contents("http://graphical.weather.gov/xml/sample_products/browser_interface/ndfdXMLclient.php?listZipCodeList=".$Zip));
        $response = explode(",",$response->latLonList);
        $Lat = $response[0];
        $Lon = $response[1];
        echo json_encode(array("Lat"=>$Lat,"Long"=>$Lon));
    break;
    
    case 'CreateZone':
        
        if(isset($_SESSION['User']))
        {
            $Name = $_GET['Name'];
            $Crop = $_GET['Crop'];
            $Variety = $_GET['Variety'];
            $Lat = $_GET['Lat'];
            $Long = $_GET['Long'];
            $Border = $_GET['Border'];
            $ZoneID = "".(uniqid (rand (),true));
            
            $db->zones->insert(array("Name"=>$Name,"Crop"=>$Crop,"Variety"=>$Variety,"Latitude"=>$Lat,"Longitude"=>$Long,"Border"=>$Border,"ZoneID"=>$ZoneID,"Nodes"=>array()));
            
            $filter = array('UserID'=>$_SESSION['User']['UserID']);
                
            $update = array('$push' => array("Zones" => $ZoneID));
            
            $db->users->update($filter,$update);
                
            //update session info
            $collection = new MongoCollection($db, 'users');
            $session = ($collection->findOne($filter));
            $_SESSION['User'] = $session;        
            
            echo json_encode("Success");
        }
        else
            echo json_encode("Fail");
            
    break;
    
    case 'DeleteZone':
        
        $ZoneID = $_GET['ZID'];
        $check = false;
        foreach($_SESSION['User'] as $key => $value)
        {
            if($key == "Zones")
            {
                foreach($value as $key2 => $value2)
                {
                    if($ZoneID == $value2)
                        $check = true;
                }
            }
        }

        if($check == true)
        { 
            $db->zones->remove(array("ZoneID"=>$ZoneID));
        
            $filter = array('UserID'=>$_SESSION['User']['UserID']);
            
            $update = array('$pull' => array("Zones" => $ZoneID));
        
            $db->users->update($filter,$update);

            //update session info
            $collection = new MongoCollection($db, 'users');
            $session = ($collection->findOne($filter));
            $_SESSION['User'] = $session;        
        
            echo json_encode("Success");
        }
        else
            echo json_encode("Fail");
            
    break;
    
    case 'GetZoneOverview':
        $ZoneID = $_GET['ZID'];
        
        if(isset($_GET['onlyCurrent']))
            $IsOnlyCurrent = $_GET['onlyCurrent'] === 'true'? true: false;
        else
            $IsOnlyCurrent = false;
            
        if(isset($_GET['onlyCurrentFull']))
            $IsOnlyCurrentFull = $_GET['onlyCurrentFull'] === 'true'? true: false;
        else
            $IsOnlyCurrentFull = false;
        
        $Start = strtotime('-4 day');
        if(isset($_GET['Start']))
        {
            if($_GET['Start'] != "")
                $Start = strtotime($_GET['Start']);
        }
        
        $End = strtotime('+1 day');
        if(isset($_GET['End']))
        {
            if($_GET['End'] != "")
                $End = strtotime($_GET['End'] . "+ 1 Day");
        }
        
        $Current = time();
        if($End < time())
            $Current = $End;
        if($Start > time())
            $Current = $Start;

        $check = false;
        foreach($_SESSION['User'] as $key => $value)
        {
            if($key == "Zones")
            {
                foreach($value as $key2 => $value2)
                {
                    if($ZoneID == $value2)
                        $check = true;
                }
            }
        }
        
        $data = array("Status"=>"Fail");
        
        if($check == true)
        {
            $country = $_SESSION['User']['Country'];
            $state = $_SESSION['User']['State'];
            $city = $_SESSION['User']['City'];
            
            //Get Zone Info
            $collection = new MongoCollection($db, 'zones');
            $filter = array('ZoneID'=>$ZoneID);
            $zone = ($collection->findOne($filter));
            
            $data["Status"] = "Success";
            $data["Name"] = $zone["Name"];
            $data["Crop"] = $zone["Crop"];
            $lat = $zone['Latitude'];
            $lon = $zone['Longitude'];
            $data["Border"] = $zone["Border"];
            
            //Get Public Data for Zone
            include 'getPublicData.php';
            $public = getPublicData($lat,$lon,$country,$state,$city,$Start,$Current,$End,$IsOnlyCurrent,$IsOnlyCurrentFull);
            $data = array_merge($public, $data);
            $data["Sensor"]="F";
            if($_SESSION['User']['Has Sensors'] == "T")
            {
                include 'getNodeData.php';
                $sensor = getNodeData($zone["Nodes"],$Start,$Current);
                $data = array_merge_recursive($sensor, $data);
                $data["Sensor"]="T";
            }
           
        }
        
        echo json_encode($data);
        
    break;
    
    case 'GetZoneBorders':
    
        $data = array("Status"=>"Fail");
        
        if(isset($_SESSION['User']))
        {
            $country = $_SESSION['User']['Country'];
            $state = $_SESSION['User']['State'];
            $city = $_SESSION['User']['City'];
            
            foreach ($_SESSION['User']['Zones'] as $ZoneID)
            {
            
                //Get Zone Info
                $collection = new MongoCollection($db, 'zones');
                $filter = array('ZoneID'=>$ZoneID);
                $zone = ($collection->findOne($filter));
                $data["Zones"][$ZoneID] = array();
                $data["Zones"][$ZoneID]["ID"] = $ZoneID;
                $data["Zones"][$ZoneID]["Name"] = $zone["Name"];
                $data["Zones"][$ZoneID]["Crop"] = $zone["Crop"];
                $data["Zones"][$ZoneID]["Border"] = $zone["Border"];
                $data["Zones"][$ZoneID]["Latitude"] = $zone['Latitude'];
                $data["Zones"][$ZoneID]["Longitude"] = $zone['Longitude'];
                foreach ($zone["Nodes"] as $NodeID)
                {
                    $dataDB = new MongoCollection($db, 'data');
                    $nodeDB = new MongoCollection($db, 'nodes');
                    
                    $filter = array('NodeID'=>$NodeID);
                    $sort = array('Time'=>-1);
                    
                    $qry = array('$query' => $filter);

                    $data["Zones"][$ZoneID]["Nodes"][$NodeID] = ($nodeDB->findOne($qry));
                    unset($data["Zones"][$ZoneID]["Nodes"][$NodeID]["NodeID"]);
                    
                    $qry = array('$query' => $filter, '$orderby' => $sort);
                    
                    $data["Zones"][$ZoneID]["Nodes"][$NodeID]["Data"] = ($dataDB->findOne($qry));
                    unset($data["Zones"][$ZoneID]["Nodes"][$NodeID]["Data"]["NodeID"]);
                    
                    if($data["Zones"][$ZoneID]["Nodes"][$NodeID]["Data"] == null)
                    {
                        $data["Zones"][$ZoneID]["Nodes"][$NodeID]["Data"]["Time"] = array("Sec" => "No Data");
                        $data["Zones"][$ZoneID]["Nodes"][$NodeID]["Data"]["Values"] = array("" => "No Data Available. Please check your probe if you do not start receiving data within one hour of installation!");
                    }
                    
                }
            }
            $data["Status"] = "Success";
           
        }
        
        echo json_encode($data);
        
    break;
    
    case 'UpdateZone':
        $ZoneID = $_GET['ZID'];
        
        if(isset($_GET['Lat']))
            $Lat = $_GET['Lat'];
        else
            $Lat = "";
        if(isset($_GET['Long']))
            $Long = $_GET['Long'];
        else
            $Long = "";
        if(isset($_GET['Name']))
            $Name = $_GET['Name'];
        else
            $Name = "";
        if(isset($_GET['Crop']))
            $Crop = $_GET['Crop'];
        else
            $Crop = "";
        if(isset($_GET['Border']))
            $Border = $_GET['Border'];
        else
            $Border = "";

        
        $check = false;
        foreach($_SESSION['User'] as $key => $value)
        {
            if($key == "Zones")
            {
                foreach($value as $key2 => $value2)
                {
                    if($ZoneID == $value2)
                        $check = true;
                }
            }
        }
        
        $out = "Fail";
        
        if($check == true)
        {
            $collection = new MongoCollection($db, 'zones');
            
            $filter = array('ZoneID'=>$ZoneID);
            
            if($Lat != "")
            {
                $update = array('$set'=>array('Latitude'=>$Lat));
                $collection->update($filter,$update);
            }
            if($Long != "")
            {
                $update = array('$set'=>array('Longitude'=>$Long));
                $collection->update($filter,$update);
            }
            if($Name != "")
            {
                $update = array('$set'=>array('Name'=>$Name));
                $collection->update($filter,$update);
            }
            if($Crop != "")
            {
                $update = array('$set'=>array('Crop'=>$Crop));
                $collection->update($filter,$update);
            }
            if($Border != "")
            {
                $update = array('$set'=>array('Border'=>$Border));
                $collection->update($filter,$update);
            }
            $out = "Success";
        }
        echo json_encode($out); 
        
    break;
    
    
    case 'LatestNodeData':
        $NodeID = $_GET['NodeID'];
        
        $check = false;
        foreach($_SESSION['User'] as $key => $value)
        {
            if($key == $NodeID)
                $check = true;
        }
        
        $data = "";
        
        if($check == true)
        {
            $collection = new MongoCollection($db, 'data');
            
            $filter = array('NodeID'=>$NodeID);
            $sort = array('Time'=>-1);
            
            $qry = array('$query' => $filter, '$orderby' => $sort);
            
            $data = ($collection->findOne($qry));    
        }
        echo json_encode($data);
        
    break;
    
    case 'HistoricalNodeData':
        $NodeID = $_GET['NodeID'];
        $Duration = $_GET['Duration'];
        $Start = $_GET['Start'];
        $End = $_GET['End'];
        
        $collection = new MongoCollection($db, 'data');
        
        $filter = array('NodeID'=>$NodeID);

        $check = true; //change back to false
        foreach($_SESSION['NodeInfo'] as $key => $value)
        {
            if($key == $NodeID)
                $check = true;
        }      
        
        $out = array();
		
		function getData($DurationString,$collection,$filter)
		{
			$now = time();
			$startTime = strtotime($DurationString, $now);
			$filter['Time'] = array('$gte'=>new MongoDate($startTime),'$lt'=>new MongoDate($now));
			$data = $collection->find($filter);
			
			$len = iterator_count($data);
			
			$chunk = $len / 200;
			
			$values = array();
            
            $time = array();
			
			$k = 0;
			
			foreach($data as $datapoint)
            {
                
                foreach($datapoint['Values'] as $Key => $ValueFromDB)
                {
                    if(!(isset($values[$Key])))
                    {
                        $values[$Key] = array("Value"=>array(),"Time"=>array());
                    }
                }
                
				if($k == 0)
				{
                    foreach($datapoint['Values'] as $Key => $ValueFromDB)
                    {
                        $tempValue[$Key] = 0.0;
                    }
                    foreach($datapoint['Time'] as $Key => $ValueFromDB)
                    {
                        if($Key == 'sec')
                            $time = 0.0;
                    }
				}
				
				foreach($datapoint['Time'] as $Key => $ValueFromDB)
				{
					if($Key == 'sec')
						$time += floatval($ValueFromDB);
				}
                
				foreach($datapoint['Values'] as $Key => $ValueFromDB)
				{
					$tempValue[$Key] += floatval($ValueFromDB);
				}
				
				$k++;
				
				if($k >= $chunk)
				{
					foreach($values as $Key => $Value)
                    {
                        $time = date("Y-m-d\TH:i:s", $time/$k);
                        $time = $time."-07:00";
                        array_push($values[$Key]["Value"],$tempValue[$Key]/$k);
                        array_push($values[$Key]["Time"],$time);
                    }
					$k = 0;
				} 
				
			}
			
			return($values);
		}
		
        if($check == true)
        {
            if($Duration == "Custom")
                $out = array("Not Supported");
            else if($Duration == "OneDay")
                $out = getData("-1 Day",$collection,$filter);
            else if($Duration == "OneWeek")
                $out = getData("-1 Week",$collection,$filter);
            else if($Duration == "OneMonth")
                $out = getData("-1 month",$collection,$filter);
            else if($Duration == "ThreeMonths")
                $out = getData("-3 months",$collection,$filter);
            else if($Duration == "SixMonths")
                $out = getData("-6 months",$collection,$filter);
            else if($Duration == "OneYear")
                $out = getData("-1 year",$collection,$filter);
            else
                $out = array("Error");
        }
   
        echo json_encode($out);
        
    break;
    
    case 'UpdateNodeName':
        
        $NodeID = $_GET['NodeID'];
        $Name = $_GET['Name'];
        
        $check = false;
        foreach($_SESSION['NodeInfo'] as $key => $value)
        {
            if($key == $NodeID)
                $check = true;
        }
        
        $update = "";
        
        if($check == true)
        {
            $collection = new MongoCollection($db, 'Zones');
            
            $filter = array('Nodes'=>array('$elemMatch' => array('ID'=>$NodeID)));
            $update = array('$set'=>array('Nodes.$.Name'=>$Name));
            
            $collection->update($filter,$update);
        }
        
        echo json_encode($update);
        
    break;
    
    case 'UpdateNodePosition':
        
        $NodeID = $_GET['NodeID'];
        $Lat = $_GET['Lat'];
        $Long = $_GET['Long'];
        
        $check = false;
        foreach($_SESSION['NodeInfo'] as $key => $value)
        {
            if($key == $NodeID)
                $check = true;
        }
        
        $update = "";
        
        if($check == true)
        {
            $collection = new MongoCollection($db, 'Zones');
            
            $filter = array('Nodes'=>array('$elemMatch' => array('ID'=>$NodeID)));
            
            $update = array('$set'=>array('Nodes.$.Latitude'=>$Lat));
            $collection->update($filter,$update);
            
            $update = array('$set'=>array('Nodes.$.Longitude'=>$Long));
            $collection->update($filter,$update);
        }
        echo json_encode($update);
        
    break;
    
    case 'Logout':
        
        session_destroy();
        echo json_encode("log out successful");
        
    break;
    
    case 'SendResetEmail':
    
        if (isset($_GET['Email']))
            $Email= strtolower(addslashes(($_GET['Email'])));
            
        $collection = new MongoCollection($db, 'users');
        $filter = array('Email'=>$Email);
        $cursor = ($collection->find($filter));
        
        //check if there is already an account with that email
        if($cursor->count() > 0)
        {
            $collection = new MongoCollection($db, 'users');
            $filter = array('Email'=>$Email);
            $out = ($collection->findOne($filter));
            
            $message = '<html><body><br>Please click here to reset your password:<br>'.$_SERVER['HTTP_HOST'].'/app/forgotpassword.html?A='.$out['UserID'].'&B='.$out['Salt'].'<br><br>Remember to share '.$_SERVER['HTTP_HOST'].' with your friends!<br><br>Regards,<br>rooT team</body></html>';
            $to      =  $Email;
            $subject = 'rooT Password Reset';
            $headers = 'From: rooT <info@project-root.com>' . "\r\n" .
                        'Reply-To: rooT <info@project-root.com>' . "\r\n" .
                        'MIME-Version: 1.0' . "\r\n" .
                        'Content-Type: text/html; charset=ISO-8859-1';

            mail($to, $subject, $message, $headers);
            
            echo json_encode("Email");
        }
        else
        {
            echo json_encode("Account Not Found");
        }
        
    break;
    
    case 'ResetPass':
    
        if (isset($_GET['UserID']))
            $UserID= $_GET['UserID'];
        if (isset($_GET['Salt']))
            $Salt= $_GET['Salt'];
        if (isset($_GET['Pass1']))
            $Pass1= addslashes($_GET['Pass1']);
        if (isset($_GET['Pass2']))
            $Pass2= addslashes($_GET['Pass2']);

        $collection = new MongoCollection($db, 'users');
        $filter = array('UserID'=>$UserID);
        $out = ($collection->findOne($filter));
        
        if($out['Salt'] == $Salt && $Pass1 == $Pass2)
        {
            
            include 'passwordhash.php';
            $hasher = new PasswordHash(10, FALSE);
            $hashed_password = $hasher->HashPassword($Pass1.$UserID.$Salt);
            $collection->update($filter,array('$set' => array("Password"=>$hashed_password)));
            
            echo json_encode("Email");
        }
        else
            echo json_encode("Error");
        
    break;
}


?>

<?php

function getPublicData($lat,$lon,$country,$state,$city,$DatePrev,$DateStart,$DateEnd,$JustCurrent,$JustCurrentFull)
{
    
    $prevdate = date("Y-m-d", $DatePrev);
    $prevtime = date("h:i:s", $DatePrev);
    $prevmongotime = new MongoDate($DatePrev);

    $startdate = date("Y-m-d", $DateStart);
    $starttime = date("h:i:s", $DateStart);
    $startmongotime = new MongoDate($DateStart);

    $enddate = date("Y-m-d", $DateEnd);
    $endtime = date("h:i:s", $DateEnd);
    $endmongotime = new MongoDate($DateEnd);

    $latlonarray = array("Latitude"=>$lat,"Longitude"=>$lon);

    try
    {
        $m = new MongoClient('mongodb://localhost');
        $db = $m->root;
    }
    catch (Exception $e) 
    {
        echo $e->getMessage();
    }

    $public_out = array();
    $notInDB = false;
    $curTime = new MongoDate(time());
    $futureTime = new MongoDate(strtotime('+1 hours'));



    ////////////

    //if latest copy not in db, get from web services

    //-----------------------------------------------------------California------------------------------------------   
    $futureStationTime = new MongoDate(strtotime('+1 month'));
        
    $APIKey = "2e221f44-fa2c-4daa-869c-8e87caa72072";
    $context = stream_context_create(array('http'=>array('method'=>"GET",'header'=>"Accept-Header: application/json")));
    
    //check if we have an up to date list of the California stations gps coords
    $collection = new MongoCollection($db, 'CIMIS_Station_List');
    $stations_fromDB = $collection->findOne();
    $databaseTime = $stations_fromDB['Timestamp'];
    
    //get latest list of stations from CIMIS
    if($curTime > $databaseTime)
    {
        $url = "http://et.water.ca.gov/api/station";
        $response = json_decode(file_get_contents($url, false, $context),true);
        
        $stations = array("Timestamp"=>$futureStationTime,"Stations"=>array());
        
        foreach ($response['Stations'] as $station)
        {
            if($station['IsActive'] == "True" && $station['IsEtoStation'] == "True")
            {
                $stationlat = explode(" / ",$station['HmsLatitude']);
                $stationlon = explode(" / ",$station['HmsLongitude']);
                $station_data = array("Number" => $station['StationNbr'], "Latitude" => $stationlat[1], "Longitude" => $stationlon[1]);
                array_push($stations["Stations"],$station_data);
            }
        }
        $db->CIMIS_Station_List->update(array(),$stations,array("upsert"=>true));
        $stations_fromDB = $collection->findOne();
    }
    //Find closest active station to coordinates
    $closestStation = "";
    $closestStationDistance = 9999999999999;
    foreach ($stations_fromDB["Stations"] as $station)
    {
        $distance = vincentyGreatCircleDistance($lat, $lon, $station["Latitude"], $station["Longitude"]);
        if($distance < $closestStationDistance)
        {
            $closestStationDistance = $distance;
            $closestStation = $station["Number"];
        }
    }
    
    $searchArray = array('Station'=>$closestStation,'Time'=>array('$gte'=>$prevmongotime,'$lte'=>$startmongotime));
    
    $collection = new MongoCollection($db, 'CIMIS_Data_Cache');
    $data = $collection->find(array('$query' => $searchArray, '$orderby' => array("Time"=>-1)));
    $toDB = array();
    
    foreach ($data as $tmp => $datatype)
    {
        $time = $datatype['Time'];
        
        foreach ($datatype as $key => $value)
        {
            if($key != "_id" and $key != "Station" and $key != "Time")
            {
                $key = mapName($key);
                if($JustCurrent and !(in_array($key,array("Air Temperature","Precipitation","Relative Humidity"))))
                {
                    //Do Nothing
                }
                else
                { 
                    if(!(isset($toDB[$key])))
                    {
                        $toDB[$key] = array();
                        $toDB[$key]['Historical'] = array();
                        $toDB[$key]['Historical']['Time'] = array();
                        $toDB[$key]['Historical']['Value'] = array();
                        $toDB[$key]['Historical']['MaxVal'] = 0;
                        $toDB[$key]['Historical']['MinVal'] = 999999999;
                    }

                    $toDB[$key]['Historical']['CurrentValue'] = $value;
                    $toDB[$key]['Historical']['Status'] = "success";
                    if(!isset($toDB[$key]['Historical']['Units']))
                        $toDB[$key]['Historical']['Units'] = getUnitFromName($key);
                    
                    if(!$JustCurrent and !$JustCurrentFull)
                    {
                        $toDB[$key]['Historical']['Group'] = getGroup($key);
                        $toDB[$key]['Historical']['DisplayType'] = displayType($key);
                        array_push($toDB[$key]['Historical']['Time'],((($time->sec)+(7*60*60))*1000));
                        array_push($toDB[$key]['Historical']['Value'],$value);
                        $toDB[$key]['Historical']['MaxVal'] = max($toDB[$key]['Historical']['MaxVal'],$value);
                        $toDB[$key]['Historical']['MinVal'] = min($toDB[$key]['Historical']['MinVal'],$value);
                    }
                }
            }
        }
        if($JustCurrent or $JustCurrentFull)
           break;
    }
    
    $toDB_Final = array();
    $toDB_Final['Data'] = $toDB;
    $public_out = array_merge_recursive($toDB_Final, $public_out);



    //---------------------------USA NATIONAL DATA------------------------------------------------------------------

    //National NOAA Forecast
    $collection = new MongoCollection($db, 'NDFD_Data_Cache');
    $data = $collection->findOne($latlonarray);

    $databaseTime = $data['Timestamp'];
    $databaseEndDate = $data['EndDate'];
    $notInDB = false;
    
    if($curTime > $databaseTime)
        $notInDB = true;
    if($endmongotime > $databaseEndDate)
        $notInDB = true;
           
    if(!$JustCurrent and !$JustCurrentFull)
    {
        if($notInDB)
        {
            $response = new SimpleXMLElement(file_get_contents("http://graphical.weather.gov/xml/sample_products/browser_interface/ndfdXMLclient.php?lat=".$lat."&lon=".$lon."&product=time-series&begin=".$startdate."T".$starttime."&end=".$enddate."T".$endtime."&maxt=maxt&mint=mint&pop12=pop12&wspd=wspd&wdir=wdir&temp=temp&qpf=qpf&dew=dew&temp=temp&maxrh=maxrh&minrh=minrh&sky=sky"));

            $responseData = json_decode(json_encode($response->data),true);

            //Get Values
            $responseTime = $responseData["time-layout"];

            //Get Time Layouts
            $timeLayouts = array();
            foreach($responseTime as $layout)
            {
                $timekey = $layout["layout-key"];
                $timearray = array();
                if(!is_array($layout["start-valid-time"]))
                    array_push($timearray,$layout["start-valid-time"]);
                else
                    $timearray = $layout["start-valid-time"];
                $timeLayouts[$timekey] = $timearray;   
            }

            $out = array();
            foreach($response->data->parameters as $parameter)
            {
                foreach ($parameter as $value)
                {
                    $temp = json_decode(json_encode($value),true);
                    
                    $temp['name'] = mapName($temp['name']);
                    
                    $out[$temp['name']]['Forecast'] = array();
                    $out[$temp['name']]['Forecast']['Status'] = "success";
                    $out[$temp['name']]['Forecast']['Group'] = getGroup($temp['name']);
                    $out[$temp['name']]['Forecast']['DisplayType'] = displayType($temp['name']);
                    
                    if(is_array($temp['value']))
                    {
                        $out[$temp['name']]['Forecast']['CurrentValue'] = $temp['value'][0];
                        $out[$temp['name']]['Forecast']['MaxValue'] = max($temp['value']);
                        $out[$temp['name']]['Forecast']['MinValue'] = min($temp['value']);
                        $out[$temp['name']]['Forecast']['Value'] = $temp['value'];
                    }
                    else
                    {
                        $out[$temp['name']]['Forecast']['CurrentValue'] = $temp['value'];
                        $out[$temp['name']]['Forecast']['MaxValue'] = $temp['value'];
                        $out[$temp['name']]['Forecast']['MinValue'] = $temp['value'];
                        $out[$temp['name']]['Forecast']['Value'] = array($temp['value']);
                    }
                        
                    foreach($value->attributes() as $a => $b)
                    {
                        $b = json_decode(json_encode($b),true);
                        if($a == "units")
                            $out[$temp['name']]['Forecast']['Units'] = getUnits($b[0]);
                        else if($a == "time-layout")
                        {
                            $out[$temp['name']]['Forecast']['Time'] = $timeLayouts[$b[0]];
                            //Convert to timestamp
                            array_walk_recursive($out[$temp['name']]['Forecast']['Time'], function(&$element) {
                              //remove the -7:00
                              $element = substr($element,0,-6);
                              $element = strtotime($element) * 1000;
                            });
                        }
                    }

                    //If we are dealing with knots, convert to MPH
                    if($out[$temp['name']]['Forecast']['Units']['Unit'] == "knots")
                    {
                        array_walk_recursive($out[$temp['name']]['Forecast']['Value'], function(&$element) {
                            $element = number_format((float)(floatval($element) * 1.15078), 2, '.', '');
                        });
                        $out[$temp['name']]['Forecast']['Units'] = getUnits("(MPH)");
                    }        
                }
            }
            
            $toDB = array("Latitude"=>$lat,"Longitude"=>$lon,"Data"=>$out,"Timestamp"=>$futureTime,"EndDate"=>$endmongotime);
            
            //$db->NDFD_Data_Cache->insert($toDB);
            $db->NDFD_Data_Cache->update($latlonarray,$toDB,array("upsert"=>true));
            
            $public_out = array_merge_recursive($toDB, $public_out);
        }
        else
        {
             $public_out = array_merge_recursive($data, $public_out);
        }

    }
    //Final ECHO out

    //Remove Data Points that are over the range
    if($endmongotime < $databaseEndDate)
    {
        foreach ($public_out['Data'] as $a => $b)
        {
            foreach($b as $key=>$value)
            {
                if($key == "Forecast")
                {
                    for ($i=0; $i<count($b[$key]['Time']); $i++)
                    {
                        if($b[$key]['Time'][$i] > $DateEnd*1000)
                        {
                            array_splice($public_out['Data'][$a][$key]['Time'],$i);
                            array_splice($public_out['Data'][$a][$key]['Value'],$i);
                        }
                    }
                }
            }
        }
    }
    
    unset($public_out['EndDate']);
    return ($public_out);

}



















function vincentyGreatCircleDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
{
  // convert from degrees to radians
  $latFrom = deg2rad($latitudeFrom);
  $lonFrom = deg2rad($longitudeFrom);
  $latTo = deg2rad($latitudeTo);
  $lonTo = deg2rad($longitudeTo);

  $lonDelta = $lonTo - $lonFrom;
  $a = pow(cos($latTo) * sin($lonDelta), 2) +
    pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
  $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

  $angle = atan2(sqrt($a), $b);
  return $angle * $earthRadius;
}

function getUnitFromName($Name)
{
    $out = "";
    switch($Name){

        case "Air Temperature":
        case "Dew Point Temperature":
        case "Soil Temperature":
            $out = "(F)";
        break;
        case "ETO":
        case "Precipitation":
            $out = "(in)";
        break;
        case "Net Radiation":
        case "Solar Radiation":
            $out = "(Ly/day)";
        break;
        case "Relative Humidity":
            $out = "(%)";
        break;
        case "Wind Speed":
            $out = "(MPH)";
        break;
        case "Vapor Pressure":
            $out = "(mBars)";
        break;
        case "Wind Direction":
            $out = "(�)";
        break;
    }
    return getUnits($out);
}

function getUnits($unit)
{
    switch($unit){

        case 'percent':
        case '(%)':
        case '%':
            return array("Unit"=>'%',"Min"=>0,"Max"=>100);
        break;
        
        case 'Fahrenheit':
        case '(F)':
            return array("Unit"=>' &deg;F',"Min"=>0,"Max"=>120);
        break;
        
        case '(in)':
        case 'inches':
            return array("Unit"=>' Inches per Hour',"Min"=>0,"Max"=>10);
        break;
        
        case '(Ly/day)':
            return array("Unit"=>' Ly/Day',"Min"=>-100,"Max"=>3000);
        break;
        
        case '(MPH)':
            return array("Unit"=>' MPH',"Min"=>0,"Max"=>30);
        break;
        
        case '(mBars)':
            return array("Unit"=>' mBars',"Min"=>0,"Max"=>100);
        break;
        
        case '(�)':
            return array("Unit"=>'�',"Min"=>0,"Max"=>360);
        break;

    }
    return array("Unit"=>$unit,"Min"=>0,"Max"=>100);
}

function displayType($theName)
{
    switch($theName){
        case 'Air Temperature':
            return "Range-Map";
        case 'Dew Point Temperature':
            return "Range";
        case 'Daily Maximum Temperature':
        case 'Daily Minimum Temperature':
            return "Point";
        break;
        
        case 'Soil Temperature':
            return "Range-Map";
        break;
        
        case 'Relative Humidity':
            return "Range-Map";
        case 'Daily Maximum Relative Humidity':
        case 'Daily Minimum Relative Humidity':
            return "Point";
        break;
        
        case 'ETO':
            return "Range-Map";
        case 'ASCE ETO':
            return "Range";
        break;
        
        case 'Net Radiation':
            return "Range";
        case 'Solar Radiation':
            return "Range";
        case 'Cloud Cover Amount':
            return "Range";
        break;
        
        case 'Resultant Wind':
            return "Vector";
        case 'Wind Direction':
            return "Direction";
        case 'Wind Speed':
            return 'Vector';
        break;
            
        case 'Precipitation':
            return "Range-Map";
        case '12 Hourly Probability of Precipitation':
            return "Probability";
        break;

        case 'Vapor Pressure':
            return "Range-Map";
        break;
        

    }
    return $theName;

}

function mapName($theName)
{
    switch($theName){
        case 'HlyAirTmp':
        case 'Temperature':
            return "Air Temperature";
        break;
        case 'HlyDewPnt':
            return "Dew Point Temperature";
        break;
        case 'HlyEto':
            return "ETO";
        break;
        case 'HlyNetRad':
            return "Net Radiation";
        break;
        case 'HlyAsceEto':
            return "ASCE ETO";
        break;
        case 'HlyPrecip':
        case 'Liquid Precipitation Amount':
            return "Precipitation";
        break;
        case 'HlyRelHum':
            return "Relative Humidity";
        break;
        case 'HlyResWind':
            return "Resultant Wind Speed";
        break;
        case 'HlySoilTmp':
            return "Soil Temperature";
        break;
        case 'HlySolRad':
            return "Solar Radiation";
        break;
        case 'HlyVapPres':
            return "Vapor Pressure";
        break;
        case 'HlyWindDir':
            return "Wind Direction";
        break;
        case 'HlyWindSpd':
            return "Wind Speed";
        break;
    }
    return $theName;
}

function getGroup($theName)
{
    switch($theName){
        case 'Air Temperature':
        case 'Dew Point Temperature':
        case 'Daily Maximum Temperature':
        case 'Daily Minimum Temperature':
        case 'Temperature':
            return "Air Temperature";
        break;
        
        case 'Soil Temperature':
            return "Soil Temperature";
        break;
        
        case 'Relative Humidity':            
        case 'Daily Maximum Relative Humidity':
        case 'Daily Minimum Relative Humidity':
            return "Humidity";
        break;
        
        case 'ETO':
        case 'ASCE ETO':
            return "ETo";
        break;
        
        case 'Net Radiation':
        case 'Solar Radiation':
            return "Radiation";
        break;
        
        case 'Cloud Cover Amount':
            return "Cloud Cover";
        break;
        
        case 'Resultant Wind Speed': 
        case 'Wind Direction':
        case 'Wind Speed':
            return 'Wind';
        break;
            
        case 'Precipitation':
        case '12 Hourly Probability of Precipitation':
        case 'Liquid Precipitation Amount':
            return "Precipitation";
        break;

        case 'Vapor Pressure':
            return "Vapor Pressure";
        break;
        

    }
    return $theName;
}
?>
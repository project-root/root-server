<?php

// Known prefix
$v4mapped_prefix_hex = '00000000000000000000ffff';
$v4mapped_prefix_bin = pack("H*", $v4mapped_prefix_hex);

try
{
        //Create "Injection Free" Connection to Server
        $dbh = new PDO("mysql:host=localhost;dbname=customer_data", 'web', 'SSBroot14');
        $dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


	$Name = $_GET['Name'];
	$Email = $_GET['Email'];
	$Type = $_GET['Type'];

	$status = 'Good';

	if (!filter_var($Email, FILTER_VALIDATE_EMAIL)) {
	    $status = 'Bad Email';
	}
	else
	{
		$addr = $_SERVER['REMOTE_ADDR'];
		$addr_bin = inet_pton($addr);
		// Check prefix
		if( substr($addr_bin, 0, strlen($v4mapped_prefix_bin)) == $v4mapped_prefix_bin) {
		  // Strip prefix
		  $addr_bin = substr($addr_bin, strlen($v4mapped_prefix_bin));
		}

		// Convert back to printable address in canonical form
		$ip = inet_ntop($addr_bin);
		$details = json_decode(file_get_contents("http://ipinfo.io/".$ip."/json"));
		$Country =  $details->country;
		$Region =  $details->region;
		$City = $details->city;
        
		$qry = $dbh->prepare("INSERT INTO data (Email, Country, Region, City) VALUES (?,?,?,?);");
		$qry->execute(array($Email,$Country,$Region,$City));
		
		$message = '<html><body>Thank you for signing up!<br><br>You will be the first to know when our product is available to the public.<br>Please feel free to email us with questions and suggestions.<br>Stay tuned for more info and share project-root.com with your friends!<br><br>Regards,<br>rooT team</body></html>';
		$to      =  $Email;
		$subject = 'Welcome from project rooT';
		$headers = 'From: project rooT <info@project-root.com>' . "\r\n" .
					'Reply-To: project rooT <info@project-root.com>' . "\r\n" .
					'MIME-Version: 1.0' . "\r\n" .
					'Content-Type: text/html; charset=ISO-8859-1';

		mail($to, $subject, $message, $headers);
		
		
	}

	$out = array();

	$out['Status'] = $status;

	echo json_encode($out);

}
catch(PDOException $e)
{
        echo($e->getMessage());
}

?>


<?php

try
{
	$input = json_decode(file_get_contents('php://input'), true);
    $m = new MongoClient('mongodb://localhost');
}
catch (Exception $e) 
{
    echo $e->getMessage();
}
    
if(!(empty( $input['Nodes'])))
{
    foreach($input['Nodes'] as $Node)
    {
        $toServer = array();
        $toServer['NodeID'] = $Node['NodeID'];
        $toServer['Values'] = $Node['Values'];
        $offset = 0;
        if(isset($Node['Time']))
        {
            $offset = ($Node['Time'] - 1) * $Node['Interval'];
        }
        if(!empty($toServer['Values']))
        {
            $time = strtotime('-'.$offset.' seconds');
            $toServer['Time'] = new MongoDate($time);
            $obj_id = $m->root->data->insert($toServer);
        }
    }
}

echo json_encode("Success");
         
?>
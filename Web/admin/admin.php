<?php

session_start();

$password = "SSBroot14";

if(md5($password) == $_GET['Pass'] || md5($password) == $_SESSION['Pass'])
{
    if(!(isset($_SESSION['Pass'])))
        $_SESSION['Pass'] = $_GET['Pass'];
    
    $Mode = $_GET['Mode'];
    
    try
    {
        $m = new MongoClient('mongodb://localhost');
        $db = $m->root;
    }
    catch (Exception $e) 
    {
        echo $e->getMessage();
    }
    
    switch($Mode){
    
        case 'Login':
            echo json_encode("success"); 
        break;
        
        case 'FakeData':
            $out = array();
            $collection = new MongoCollection($db, 'zones');
            $filter = array("ZoneID" => $_GET["ZID"]);
            $nodes = $collection->findOne($filter);
            $nodes = $nodes['Nodes'];
            $var = intval($_GET['variation']);
            
            if(($_GET['startTime']) != "")
                $startTime = strtotime($_GET['startTime']);
            else
                $startTime =strtotime("-15 minutes");
            
            if(($_GET['endTime']) != "")
                $endTime = strtotime($_GET['endTime']);
            else
                $endTime = strtotime('now');
            
            $startTime =  strtotime(" -8 hours",$startTime);
            $endTime =  strtotime(" -8 hours",$endTime);
            
            $time = $startTime;
            
            $pattern = split("," , $_GET['baseValues']);
            $patternCounter = 0;
            
            while($endTime >= $time && (count($pattern) > 0))
            {
				foreach ($nodes as $node)
				{
					$fakeData = array();
					$fakeData['NodeID'] = $node;
					$fakeData['Values'] = array("Moisture"=>mt_rand(max(0,$pattern[$patternCounter]-$var), min(100,$pattern[$patternCounter]+$var))/100,"Air Temp"=> rand (0,100),"Ground Temp"=>rand (0,100), "Humidity" => rand(0,100));
					$fakeData['Time'] = new MongoDate($time);
					array_push($out,$fakeData);
				}
				//add 15 minutes
				$time = strtotime("+15 minutes", $time);
				$patternCounter = ($patternCounter + 1) % count($pattern);
			}
            
            $collection = new MongoCollection($db, 'data');
            if($_GET['clear'] == 'true')
            {
                $filter = array('Time' => array('$gte' => new MongoDate($startTime), '$lte' => new MongoDate($endTime)));
                $collection->remove($filter);
            }
            $collection->batchInsert($out);
            echo json_encode($out); 
        break;
        
        case 'UserCount':
            $collection = new MongoCollection($db, 'users');
            $out = $collection->count();
                
            echo json_encode($out); 
        break;
        
        case 'GetAllUsers':
            $collection = new MongoCollection($db, 'users');
            $data = $collection->find(array(),array("Name"=>1,"Email"=>1,"_id"=>0));
            $out = iterator_to_array($data);
            echo json_encode($out);
        break;
        
        case 'UserLookup':
            $collection = new MongoCollection($db, 'users');
            $regex = new MongoRegex("/".$_GET["UserInfo"]."/i");
            $filter = array('$or'=>array(array("Name"=>$regex),array("Email"=>$_GET["UserInfo"])));
            $out = $collection->find($filter);
            $out = iterator_to_array($out);
            echo json_encode($out);
        break;
        
        case 'GetCrops':
            $collection = new MongoCollection($db, 'crops');
            $data = $collection->find(array(),array("Crop Name"=>1,"Crop Variety"=>1,"_id"=>0))->sort(array('Crop Name'=>1));
            
            $out = array();
            
            foreach ($data as $row)
                array_push($out,$row);
                
            echo json_encode($out); 
            
        break;
        
        case 'CropDetails':
            $collection = new MongoCollection($db, 'crops');
            $data = $collection->find(array("Crop Name"=>$_GET['Crop'],"Crop Variety"=>$_GET['Variety']));
            
            $out = array();
            
            foreach ($data as $row)
                array_push($out,$row);
                
            echo json_encode($out); 
            
        break;
        
        case 'AddCrop':
            $collection = new MongoCollection($db, 'crops');
            $collection->insert(array("Crop Name"=>$_GET["CropName"],"Crop Variety"=>"Generic"));
            echo json_encode("success");
        break;
        
        case 'DeleteCrop':
            $collection = new MongoCollection($db, 'crops');
            $collection->remove(array("Crop Name"=>$_GET["CropName"],"Crop Variety"=>$_GET["CropVariety"]));
            echo json_encode("success");
        break;
        
        case 'UpdateCrop':
            $collection = new MongoCollection($db, 'crops');
            
            $CropName = $_GET["CropName"];
            $CropVariety = $_GET["CropVariety"];
            $StageDays = explode(",",$_GET["StageDays"]);
            $StageNames = explode(",",$_GET["StageNames"]);
            $MaxAT = explode(",",$_GET["MaxAT"]);
            $MinAT = explode(",",$_GET["MinAT"]);
            $MaxST = explode(",",$_GET["MaxST"]);
            $MinST = explode(",",$_GET["MinST"]);
            $MaxRH = explode(",",$_GET["MaxRH"]);
            $MinRH = explode(",",$_GET["MinRH"]);
            $RootDepth = explode(",",$_GET["RootDepth"]);
            $ET = explode(",",$_GET["ET"]);
            $MAD = explode(",",$_GET["MAD"]);
            $Annual = $_GET["Annual"];
            
            $count = count($StageDays);
            
            if( (count($StageDays) == $count ) &&  (count($StageNames) == $count ) &&  (count($MaxAT) == $count ) &&  (count($MinAT) == $count ) &&  (count($MaxST) == $count ) &&  (count($MinST) == $count ) &&  (count($MaxRH) == $count ) &&  (count($MinRH) == $count ) &&  (count($RootDepth) == $count ) &&  (count($ET) == $count ) &&  (count($MAD) == $count ) )
            {
            
                $updateArray = array("Crop Name"=>$CropName,"Crop Variety"=>$CropVariety,"Stage Names"=>$StageNames,"Stage Dates"=>$StageDays,"Min Air Temperature"=>$MinAT,"Max Air Temperature"=>$MaxAT,"Min Soil Temperature"=>$MinST,"Max Soil Temperature"=>$MaxST,"Min Rh"=>$MinRH,"Max Rh"=>$MaxRH,"Root Depth"=>$RootDepth,"ET Coefficient"=>$ET,"MAD"=>$MAD,"Annual" => $Annual);
            
                $collection->update(array("Crop Name"=>$CropName,"Crop Variety"=>$CropVariety),$updateArray,array("upsert" => true));
                echo json_encode("success");
            }
            else
            {
                echo json_encode("fail");
            }
        break;
        
        case 'AddNode':
            $collection = new MongoCollection($db, 'nodes');
            $collection->insert(array("NodeID"=>$_GET["nodeID"],"Name"=>$_GET["nodeName"],"Latitude"=>$_GET["lat"],"Longitude"=>$_GET["lon"]));
            
            $collection = new MongoCollection($db, 'zones');
            $filter = array("ZoneID" => $_GET["nodeZoneID"]);
            $update = array('$push' => array("Nodes" => $_GET["nodeID"]));
            $collection->update($filter, $update);
            
            echo json_encode("success");
        break;
        
        case 'NodeInfo':
            $collection = new MongoCollection($db, 'data');
            $nodeID = $_GET["nodeID"];
            $out = $collection->find(array("NodeID"=>$nodeID),array("Values"=>1,"Time"=>1,"_id"=>0));
            $out->sort(array("Time"=>-1));
            $out->limit(1);
            $out = iterator_to_array($out);
            echo json_encode($out[0]);   
        break;
        
    case 'GetZoneBorders':
    
        $data = array("Status"=>"Fail");

        
        //Get Zone Info
        $collection = new MongoCollection($db, 'zones');
        if($_GET['ZID'] != "")
            $filter = array('ZoneID'=>$_GET['ZID']);
        else if($_GET['Crop'] != "")
            $filter = array('Crop'=>$_GET['Crop']);
        else
            $filter = array();
            
        $zone = ($collection->find($filter));
        $i = 0;
        foreach ($zone as $value)
        {
            $ZoneID = $value["ZoneID"];
            
            $data["Zones"][$ZoneID] = array();
            $data["Zones"][$ZoneID]["ID"] = $value["ZoneID"];
            $data["Zones"][$ZoneID]["Name"] = $value["Name"];
            $data["Zones"][$ZoneID]["Crop"] = $value["Crop"];
            $data["Zones"][$ZoneID]["Border"] = $value["Border"];
            $data["Zones"][$ZoneID]["Lat"] = $value["Latitude"];
            $data["Zones"][$ZoneID]["Lon"] = $value["Longitude"];
            
            foreach ($value["Nodes"] as $NodeID)
            {
                $dataDB = new MongoCollection($db, 'data');
                $nodeDB = new MongoCollection($db, 'nodes');
                
                $filter = array('NodeID'=>$NodeID);
                $sort = array('Time'=>-1);
                
                $qry = array('$query' => $filter);
                
                $data["Zones"][$ZoneID]["Nodes"][$NodeID] = ($nodeDB->findOne($qry));
                unset($data["Zones"][$ZoneID]["Nodes"][$NodeID]["NodeID"]);
                
                $qry = array('$query' => $filter, '$orderby' => $sort);
                
                $data["Zones"][$ZoneID]["Nodes"][$NodeID]["Data"] = ($dataDB->findOne($qry));
                unset($data["Zones"][$ZoneID]["Nodes"][$NodeID]["Data"]["NodeID"]);
            }
        }

        
        $data["Status"] = "Success";

        
        echo json_encode($data);
        
    break;

    }

	
}
else
{
	echo json_encode("fail");
}
?>


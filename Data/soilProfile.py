import math
import requests
import re
import datetime
import helpers
import pandas as pd
from bs4 import BeautifulSoup
from pymongo import MongoClient

class soilProfile(object):

    def __init__(self, ZoneID, Nodes, rootDepth = 10, cropMad = 50, StartTime=(datetime.datetime.now() - datetime.timedelta(minutes=15)), EndTime=datetime.datetime.now()):
        self._helper = helpers.zoneDateRange(ZoneID,StartTime,EndTime)
        self.ZoneID = self._helper.ZoneID
        self.rootDepth = rootDepth
        self.cropMad = cropMad
        self.Nodes = Nodes
        self.Profile = self.getProfile()
        
    def getProfile(self):
        
        client = MongoClient('mongodb://localhost')
        db = client.root
        
            
        #function to get a list of nodes from a zone
        def getNodeCoords(listOfNodes):
            data = db.nodes
            out = []
            for data in data.find({"NodeID":{"$in":listOfNodes}}):
                out.append({"Latitude":data["Latitude"],"Longitude":data["Longitude"],"NodeID":data['NodeID']})
            return out
            
        listOfNodes = self.Nodes
        nodeValues = {}
        
        for coord in getNodeCoords(listOfNodes):
            profile = getSoilPoints(client,coord["Latitude"],coord["Longitude"],self.rootDepth,self.cropMad)
            nodeValues[coord["NodeID"]] = profile.getData()
        
        client.close()
        
        #if there was no data returned, clear out the nodes so we dont create an empty index
        if not nodeValues:
            listOfNodes = []
        
        nodeFrame = pd.DataFrame(index=listOfNodes,columns=("Soil Type","Percent Clay","Percent Sand","Percent Silt","PWP","FC","SAT","KSAT","PAW","BD","MAD","Conversion Factor"))
        
        for node,val in nodeValues.iteritems():
            nodeFrame.loc[node] = val["Soil Type"],val["Percent Clay"],val["Percent Sand"],val["Percent Silt"],val["PWP"],val["FC"],val["SAT"],val["KSAT"],val["PAW"],val["BD"],val["MAD Point"],val["Conversion Factor"]
        
        fieldMad = nodeFrame['MAD'].max()
        fieldFC = nodeFrame['FC'].min()
        fieldPWP = nodeFrame['PWP'].max()
        fieldSAT = nodeFrame['SAT'].min()
        conversionFactor = nodeFrame['Conversion Factor'].mean()
        
        return {"Values":nodeFrame,"MAD":fieldMad,"FC":fieldFC,"PWP":fieldPWP,"SAT":fieldSAT,"Conversion Factor":conversionFactor}

class getSoilPoints(object):
    
    #lat, lon, and root depth in cm
    def __init__(self,client,lat=38.712404580594196,lon=-121.8638277053833,root_depth=10,crop_mad=50,nodeID=123):
        self.lat = lat
        self.lon = lon
        self.root_depth = int(root_depth)
        self.crop_mad = float(crop_mad)/100
        self.client = client
    
    def getData(self):
        
        def downloadData():
            page = requests.get("http://casoilresource.lawr.ucdavis.edu/gmap/get_mapunit_data.php?lat="+str(self.lat)+"&lon="+str(self.lon))
            soup = BeautifulSoup(page.content)
            cokey = soup.find('a').get('data-url').split('cokey=')[1]
            avaliableWaterStorage = float(soup.find(text=re.compile("Available Water Storage")).parent.nextSibling.nextSibling.text.strip().split(" ")[0])
            page = requests.get("http://casoilresource.lawr.ucdavis.edu/soil_web/property_with_depth_table.php?cokey="+cokey)
            soup = BeautifulSoup(page.content)
            percent_clay = 0.0
            percent_sand = 0.0
            for row in soup.find('table').findAll('tr'):
            
                first_column = row.find('td').contents[0]
                
                if(str(first_column) == "Depth Range (cm)"):
                    continue
                if(row.findAll('td')[2:3][0].contents[0] == ""):
                    break
                    
                percent_clay = float(row.findAll('td')[2:3][0].contents[0])
                percent_sand = float(row.findAll('td')[3:4][0].contents[0])
                first_column = first_column.split(' - ')
                low = int(first_column[0])
                high = int(first_column[1])
                
                if((low < self.root_depth) and (self.root_depth <= high)):
                    break
            return [percent_clay,percent_sand,avaliableWaterStorage]
        
        db = self.client.root
        soildata = db.soildata
        percent_clay = 0.0
        percent_sand = 0.0
        
        qry = soildata.find({'Latitude':self.lat,'Longitude':self.lon,'Depth':self.root_depth})
        
        for data in qry:
            percent_clay = data['Percent Clay']
            percent_sand = data['Percent Sand']
            avaliableWaterStorage = data['Avaliable Water Storage']
            
        if percent_clay == 0 and percent_sand == 0:
            data = downloadData()
            percent_clay = data[0]
            percent_sand = data[1]
            avaliableWaterStorage = data[2]
            soildata.insert({'Latitude':self.lat,'Longitude':self.lon,'Percent Clay':percent_clay,'Percent Sand':percent_sand,'Avaliable Water Storage':avaliableWaterStorage,'Depth':self.root_depth})
        
        
        PWP   =0.0  # Perm. Wilt. Point (cm3/cm3) % expressed as a decimal
        FC    =0.0  # Field Capacity    (cm3/cm3) % expressed as a decimal
        SAT   =0.0  # Saturation  (cm3/cm3) % expressed as a decimal
        KSAT  =0.0  # Saturated hydraulic conductivity  cm/hr
        PAW_inch_foot   =0.0  # Plant Available Water   in/foot %
        PAW   =0.0  # Plant Available Water   (cm3/cm3) % expressed as a decimal
        BD    =0.0  # Bulk density (g/cm3)
        MAD_POINT =0.0
        
        sand_2 = percent_sand * percent_sand # sand squared
        clay_2 = percent_clay * percent_clay # clay squared 

        texturelabel = 'unknown'
        outsidebounds = 0
        showname = 0
        percent_total = 0
        percent_silt = 0

        percent_total = (percent_sand + percent_clay)
        percent_silt = 100 - percent_total

        acoef = math.exp(-4.396 - 0.0715 * percent_clay - 4.88e-4 * sand_2 - 4.285e-5 * sand_2 * percent_clay)
        bcoef = - 3.140 - 0.00222 * clay_2 - 3.484e-5 * sand_2 * percent_clay
        
        SAT = 0.332 - 7.251e-4 * percent_sand + 0.1276 * math.log10(percent_clay)
        
        if ((acoef != 0.0) and (bcoef != 0.0)):
            FC   = math.pow((0.3333/ acoef),(1.0 / bcoef))
            PWP  = math.pow((15.0  / acoef),(1.0 / bcoef))

        if (SAT != 0.0):
            KSAT = math.exp((12.012 - 0.0755 * percent_sand)  +  (- 3.895 + 0.03671 * percent_sand - 0.1103  * percent_clay + 8.7546e-4 * clay_2) / SAT)

        PAW = (FC - PWP)
        PAW_inch_foot = PAW * 12.0   # convert to in/foot

        BD = (1 -  SAT) * 2.65
        
        MAD_POINT = FC - (self.crop_mad * (FC - PWP))

        if ((percent_clay >= 40) and (20 <= percent_sand) and (percent_sand <= 45)):
            texturelabel = 'Clay'
        elif ((percent_clay >= 27) and (20 <= percent_sand) and (percent_sand <= 45)):
            texturelabel = 'Clay loam'
        elif ((percent_silt <= 40) and (percent_sand <= 20)):
            texturelabel = 'Clay'
        elif ((percent_silt > 40) and (percent_clay >= 40)):
            texturelabel = 'Silty clay'
        elif ((percent_clay >= 35) and (percent_sand >= 45)):
            texturelabel = 'Sandy clay'
        elif ((percent_clay >= 27) and (percent_sand < 20)):
            texturelabel = 'Silty clay loam'
        elif ((percent_clay <= 10) and (percent_silt >= 80)):
            texturelabel = 'Silt'
        elif (percent_silt >= 50):
            texturelabel = 'Silt loam'
        elif ((percent_clay >= 7) and (percent_sand <= 52) and (percent_silt >= 28)):
            texturelabel = 'Loam'
        elif (percent_clay >= 20):
            texturelabel = 'Sandy clay loam'
        elif (percent_clay >= percent_sand - 70):
            texturelabel = 'Sandy loam'
        elif (percent_clay >= (2 * percent_sand) - 170):
            texturelabel = 'Loamy sand'
        else:
            texturelabel = 'Sand'

        return {"Percent Clay":percent_clay,"Percent Sand":percent_sand,"Percent Silt":percent_silt,"PWP":PWP,"FC":FC,"SAT":SAT,"KSAT":KSAT,"PAW":PAW,"Soil Type":texturelabel,"BD":BD,"MAD Percent":self.crop_mad,"MAD Point":MAD_POINT,"Conversion Factor":avaliableWaterStorage/PAW}
        
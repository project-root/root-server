<?php

//Usage --  php test.php

echo ("\n\r" . "\n\r");
echo ("----------------------" . "\n\r");
echo ("Benchmark - PHP" . "\n\r");
echo ("----------------------" . "\n\r");

//Print Current Time
$startTime = intval(microtime(true)*1000);
echo("Start Time: " . (intval(microtime(true)*1000) - $startTime) . "\n\r");

//Generate 1,000,000 random numbers

echo("Generating 1 Million Random Numbers" . "\n\r");
$taskStart = intval(microtime(true)*1000);

$maxVal = 1024;
$randArray = array_fill(0, $maxVal, NULL);
$size = 1000000;

for ($i = 0; $i < $size; $i++) {
	$randArray[$i] = mt_rand(0, $maxVal);
}

echo("Task Time: " . (intval(microtime(true)*1000) - $taskStart) . "\n\r");
echo("Total Time: " . (intval(microtime(true)*1000) - $startTime) . "\n\r");
echo ("\n\r");

//sort the array
echo("Sorting Array" . "\n\r");
$taskStart = intval(microtime(true)*1000);
sort($randArray);
echo("Task Time: " . (intval(microtime(true)*1000) - $taskStart) . "\n\r");
echo("Total Time: " . (intval(microtime(true)*1000) - $startTime) . "\n\r");
echo ("\n\r");

//Find Average
echo("Finding Average" . "\n\r");
$taskStart = intval(microtime(true)*1000);
function average($sample){
	$mean = array_sum($sample) / count($sample);
	foreach($sample as $key => $num) $devs[$key] = pow($num - $mean, 2);
	return array("Mean"=>$mean,"Deviation"=>sqrt(array_sum($devs) / (count($devs) - 1)));
}
$results = average($randArray);
echo("Average: " . $results["Mean"] . "\n\r");
echo("Std. Dev: " . $results["Deviation"] . "\n\r");
echo("Task Time: " . (intval(microtime(true)*1000) - $taskStart) . "\n\r");
echo("Total Time: " . (intval(microtime(true)*1000) - $startTime) . "\n\r");
echo ("\n\r");











?>

import time
import numpy as np
from pymongo import MongoClient
import json

#Usage --  python test.py
print ("")
print ("----------------------")
print ("Benchmark - Python with NumPy")
print ("----------------------")

client = MongoClient('mongodb://localhost')
db = client.root
data = db.test

#Print Current Time
def milltime(): return int(time.clock()*1000)

startTime = milltime()
print ("Start Time: " + str(milltime() - startTime))

#Generate 1,000,000 random numbers

print ("Generating 1 Million Random Numbers")

taskStart = milltime()

maxVal = 1024
size = 1000000

randArray = np.random.randint(0, maxVal, size )

print ("Task Time: " + str(milltime() - taskStart))
print ("Total Time: " + str(milltime() - startTime))
print ("")

#select from mongodb
print ("Collecting Data")
taskStart = milltime()
list = []
for data in data.find():
    try:
        list.append(int(data["Value"]))
    except:
        pass
randArray = np.array(list)
print ("Task Time: " + str(milltime() - taskStart))
print ("Total Time: " + str(milltime() - startTime))
print ("")

#sort the array
print ("Sorting Array")
taskStart = milltime()
np.sort(randArray)
print ("Task Time: " + str(milltime() - taskStart))
print ("Total Time: " + str(milltime() - startTime))
print ("")

#find average
print ("Find Average")
taskStart = milltime()
print ("Average: " + str(np.mean(randArray)))
print ("Std. Dev: " + str(np.std(randArray,dtype=np.float64)))
print ("Task Time: " + str(milltime() - taskStart))
print ("Total Time: " + str(milltime() - startTime))
print ("")

#remove outliers

print ("Remove Outliers")
taskStart = milltime()
def reject_outliers(data, m=2):
    return data[abs(data - np.mean(data)) < m * np.std(data)]
randArray = reject_outliers(randArray,2)
print ("Task Time: " + str(milltime() - taskStart))
print ("Total Time: " + str(milltime() - startTime))
print ("")


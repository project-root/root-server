
//Usage --  node test.js
console.log(""); 
console.log("----------------------"); 
console.log("Benchmark - Node.js"); 
console.log("----------------------");

var MongoClient = require('mongodb').MongoClient;

var db = MongoClient.connect('mongodb://localhost/root', function(err, db) {
    if(err)
        throw err;
    var data = db.collection('test');

    //Print Current Time
    var startTime = Date.now(), taskStart;
    console.log("Start Time: " + (Date.now() - startTime));


    //Generate 1,000,000 random numbers

    console.log("Generating 1 Million Random Numbers");
    taskStart = Date.now();

    var maxVal = 1024, size = 1000000, randArray = new Array();

    for (var i = 0; i < size; i++) {
      randArray[i] = (Math.floor(Math.random()*maxVal));
    }

    console.log("Task Time: " + (Date.now() - taskStart));
    console.log("Total Time: " + (Date.now() - startTime));
    console.log("");
    
    //select from mongodb
    console.log("Collecting Data");
    taskStart = Date.now();
    var cursor = data.find(), list = new Array();
    
    cursor.each(function(err,item){
        if(item == null){
            processDocs(list);
            db.close();
        }
        else
            list.push(parseInt(item.Value));
    });
    
    function processDocs(array) {
        var randArray = array;
        console.log("Task Time: " + (Date.now() - taskStart));
        console.log("Total Time: " + (Date.now() - startTime));
        console.log("");

        //sort the array
        console.log("Sorting Array");
        taskStart = Date.now();
        randArray.sort()
        console.log("Task Time: " + (Date.now() - taskStart));
        console.log("Total Time: " + (Date.now() - startTime));
        console.log("");

        //Find Average
        console.log("Finding Average and Standard Dev");
        taskStart = Date.now();
        function average(a) {
            var r = {mean: 0, variance: 0, deviation: 0}, t = a.length;
            for(var m, s = 0, l = t; l--; s += a[l]);
            for(m = r.mean = s / t, l = t, s = 0; l--; s += Math.pow(a[l] - m, 2));
            return r.deviation = Math.sqrt(r.variance = s / t), r;
        }
        var results = average(randArray);
        console.log("Average: " + results.mean);
        console.log("Std. Dev: " + results.deviation);
        console.log("Task Time: " + (Date.now() - taskStart));
        console.log("Total Time: " + (Date.now() - startTime));
        console.log("");

        //Remove Outliers
        console.log("Remove Outliers");
        taskStart = Date.now();
        remove_outliers = function(a) {
            var r = average(a), m = r.mean, s = r.deviation, out = new Array();
            for(var i = 0; i<a.length; i++) {
                if(Math.abs(a[i]-m) < m * s) out.push(a[i]);
            }
            return out;
        }
        var randArray = remove_outliers(randArray);
        console.log("Task Time: " + (Date.now() - taskStart));
        console.log("Total Time: " + (Date.now() - startTime));
        console.log("");
    };
    
});
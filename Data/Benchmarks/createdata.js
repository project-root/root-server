var MongoClient = require('mongodb').MongoClient;

var db = MongoClient.connect('mongodb://localhost/root', function(err, db) {
    console.log("");
    console.log("Generating Data");
    var args = process.argv.slice(2);
    var num = 1000;
    if(typeof args[0] != 'undefined'){
        num = parseInt(args[0]);
    }
    data = new Array();
    for(var i = 0; i < num; i++)
    {
        data[i] = {"Value":(Math.floor(Math.random()*100))};
    }
    var collection = db.collection('test');
    collection.drop(function(){
        collection.insert(data,{},function(){db.close()});
    });
});
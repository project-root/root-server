import requests
import csv
from bs4 import BeautifulSoup

states = ["AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DC", "DE", "FL", "GA", 
          "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", 
          "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", 
          "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", 
          "SD", "TN", "TX", "UT", "VT", "VA", "WA", "WV", "WI", "WY"]

for stateName in states:

	print "Start: " + stateName
	payload = {'UsState': stateName,'Sord':'Farmname'}

	url = "http://www2.wilson.edu/csasearch/results.asp?pageNumber=0"

	r = requests.post(url, data=payload)

	cookies = r.cookies

	url = "http://www2.wilson.edu/csasearch/detail.asp?itemNumber="
	final = []
	done = False
	for i in xrange(0,1000):
		if done:
			break
		r = requests.post(url+str(i),cookies=cookies)
		soup = BeautifulSoup(r.content)
		out = {}
		for obj in soup.find('body').find('table').find('table').findAll('tr'):
			if("Farmname" in obj.text.encode('utf-8').strip()):
				out["Farmname"] = obj.text.encode('utf-8')[12:-1].strip()
				print out["Farmname"]
				if(len(out["Farmname"]) < 1):
					done = True
					break
			elif("Contact Name:" in obj.text.encode('utf-8').strip()):
				out["Contact Name"] = obj.text.encode('utf-8')[16:-1].strip()
			elif("Email:" in obj.text.encode('utf-8').strip()):
				out["Email"] = obj.text.encode('utf-8')[9:-1].strip()
			elif("Phone1" in obj.text.encode('utf-8').strip()):
				out["Phone"] = obj.text.encode('utf-8')[10:-1].strip()
			elif("Areas Served:" in obj.text.encode('utf-8').strip()):
				out["Areas Served"] = obj.findNext('tr').find('td').find('table').find('tr').text.encode('utf-8').strip()
			elif("Comments:" in obj.text.encode('utf-8').strip()):
				out["Comments"] = obj.findNext('tr').find('td').find('table').find('tr').text.encode('utf-8').strip()
			elif("Address:" in obj.text.encode('utf-8').strip()):
				line = obj.findNext('tr')
				string = ""
				while line.find("td").text.encode('utf-8')[1:-1].strip():
					string += line.text.encode('utf-8')[1:-1].strip() + ", "
					line = line.findNext('tr')
				out["Address"] = string
		final.append(out)

	keys = ['Farmname', 'Contact Name', 'Email', 'Phone', 'Areas Served','Comments','Address']
	f = open(stateName + '.csv', 'wb')
	dict_writer = csv.DictWriter(f, keys)
	dict_writer.writer.writerow(keys)
	dict_writer.writerows(final)
	f.close()
	print "Done: " + stateName

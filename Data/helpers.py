import datetime
from pymongo import MongoClient

class zoneDateRange(object):
    
    #default constructor is in 15 minute increment
    def __init__(self, ZoneID, StartTime, EndTime):
        if (type(StartTime) is not datetime.datetime) or (type(EndTime) is not datetime.datetime):
            print type(StartTime)
            print type(EndTime)
            raise TypeError("StartTime and EndTime must be a datetime.datetime")
        if (StartTime >= EndTime):
            raise ValueError("StartTime is later than or equal to EndTime")
        
        client = MongoClient('mongodb://localhost')
        db = client.root
        data = db.zones      
        if data.find({"ZoneID":ZoneID}).count() == 0:
            raise ValueError("ZoneID not Found")
        client.close()
        self._ZoneID = ZoneID
        self._StartTime = StartTime
        self._EndTime = EndTime
    
    @property
    def ZoneID(self):
        return self._ZoneID
        
    @ZoneID.setter
    def ZoneID(self,ZoneID):
        if len(ZoneID) != 32:
            raise ValueError("ZoneID must be 32 characters long")
        self._zoneID = ZoneID
        
    @property
    def StartTime(self):
        return self._StartTime
    
    @StartTime.setter
    def StartTime(self, StartTime):
        if type(StartTime) is not datetime.datetime:
            raise TypeError("StartTime must be a datetime.datetime")
        if (StartTime >= self._EndTime):
            raise ValueError("StartTime is later than or equal to EndTime")
        self._StartTime = StartTime
        
    @property    
    def EndTime(self):
        return self._EndTime
    
    @EndTime.setter
    def EndTime(self, EndTime):
        if type(EndTime) is not datetime.datetime:
            raise TypeError("EndTime must be a datetime.datetime")
        if (self._StartTime >= EndTime):
            raise ValueError("StartTime is later than or equal to EndTime")
        self._EndTime = EndTime
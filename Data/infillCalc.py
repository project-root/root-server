from __future__ import division

import math
import datetime
import helpers
import pandas as pd
from pymongo import MongoClient
import currentZoneValues
import csv

class infillCalc(object):

    def __init__(self, ZoneID, Nodes, StartTime=datetime.datetime.now(), EndTime=datetime.datetime.now() + datetime.timedelta(minutes=15)):
        self._helper = helpers.zoneDateRange(ZoneID,StartTime,EndTime)
        self.ZoneID = self._helper.ZoneID        
        self.StartTime = self._helper.StartTime
        self.EndTime = self._helper.EndTime
        self.Nodes = Nodes
        self.Rate = self.getRate()
        self.infillRate = self.Rate[0]
        self.depletionRate = self.Rate[1]
        
    def getRate(self):
        
        goingUp = True
        upvalues = []
        downvalues = []
        window = []
        prevAvg = 1000
        startTime = self.StartTime
        
        '''ofile  = open('soilmoisture.csv', "wb")
        writer = csv.writer(ofile, delimiter='\t', quotechar='"', quoting=csv.QUOTE_ALL)
        writer.writerow(['Time','Moisture'])
        
        while startTime > self.StartTime - datetime.timedelta(days=3):
            endTime = startTime
            startTime = startTime - datetime.timedelta(minutes=15)
            
            val = currentZoneValues.currentZoneValues(self.ZoneID,self.Nodes,startTime,endTime).ZoneValues["Moisture"]
            
            #build up window
            if len(window) < 4:
                window.append(val)
                
            else:
                average =  sum(window) / len(window)
                print average
                print window
                if prevAvg < average:
                    goingUp = False
                
                prevAvg = average
                window.append(val)
                window.pop(0)
                writer.writerow([str(startTime),str(average)])
            
        goingUp = True    
        startTime = self.StartTime
        values = []
        window = []
        prevAvg = 1000'''
        
        while goingUp:
            endTime = startTime
            startTime = startTime - datetime.timedelta(minutes=15)
            
            val = currentZoneValues.currentZoneValues(self.ZoneID,self.Nodes,startTime,endTime).ZoneValues["Moisture"]
            
            #build up window
            if len(window) < 4:
                window.append(val)
                
            else:
                prevAvg = sum(window) / len(window)
                window.append(val)
                window.pop(0)
                average =  sum(window) / len(window)

                if prevAvg > average:
                    goingUp = False
                    upvalues.append(average)
                else:
                    downvalues.append(average)
        
        while not goingUp:
            endTime = startTime
            startTime = startTime - datetime.timedelta(minutes=15)
            
            val = currentZoneValues.currentZoneValues(self.ZoneID,self.Nodes,startTime,endTime).ZoneValues["Moisture"]
            
            prevAvg = sum(window) / len(window)
            window.append(val)
            window.pop(0)
            average =  sum(window) / len(window)

            if prevAvg <= average:
                goingUp = True
            else:
                upvalues.append(average)
        
        #If our goingUp frame is too small, re-run on the previous dataset
        if len(downvalues) < 4:
            downvalues = []
            while goingUp:
                endTime = startTime
                startTime = startTime - datetime.timedelta(minutes=15)
                
                val = currentZoneValues.currentZoneValues(self.ZoneID,self.Nodes,startTime,endTime).ZoneValues["Moisture"]
                
                #build up window
                if len(window) < 4:
                    window.append(val)
                    
                else:
                    prevAvg = sum(window) / len(window)
                    window.append(val)
                    window.pop(0)
                    average =  sum(window) / len(window)

                    if prevAvg > average:
                        goingUp = False
                        upvalues.append(average)
                    else:
                        downvalues.append(average)
        
        
        uprun = len(upvalues)
        downrun = len(downvalues)

        if(uprun > 0 and downrun > 0):
            uprise = max(upvalues) - min(upvalues)
            downrise = max(downvalues) - min(downvalues)
            return [uprise/uprun,downrise/downrun]
        else:
            return [0,0]
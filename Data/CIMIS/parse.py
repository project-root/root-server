import csv
from dateutil.parser import parse
from datetime import *
import os
import glob
from pymongo import MongoClient

listOfFiles = glob.glob(os.getcwd()+"/*.csv")

for item in listOfFiles:
    print item

    client = MongoClient('mongodb://localhost')
    db = client.root

    with open(str(item), 'rb') as f:
        datapoints = []
        reader = csv.reader(f)
        for row in reader:
            station = row[0].lstrip("0")
            time = str(int(row[2])/100)
            
            if(time == "24"):
                row[1] = datetime.strptime(row[1], "%m/%d/%Y")
                row[1] = row[1] + timedelta(days=1)
                row[1] = row[1].strftime("%m/%d/%Y")
                time = "00"
            
            time = row[1]+" "+time+":00:00"
            time = parse(time)
            
            i = 5
            
            try:
                float(row[4].strip())
                i = 4
            except ValueError:
                if(row[4].strip() == '--'):
                    i = 4
                else:
                    i = 5     
            
            ETO = row[i].strip()
            i += 2
            Rain = row[i].strip()
            i += 2
            SolarRad = row[i].strip()
            i += 2
            VaporPressure = row[i].strip()
            i += 2
            AirTemp = row[i].strip()
            i += 2
            RelHumid = row[i].strip()
            i += 2
            DewPoint = row[i].strip()
            i += 2
            WindSpeed = row[i].strip()
            i += 2
            WindDir = row[i].strip()
            i += 2
            SoilTemp  = row[i].strip()
            
            if(AirTemp == "Y"):
                print "ERROR"
                
            if(AirTemp != "" and AirTemp is not None):
                datapoints.append({"Time":time,"Station":station,"HlyAirTmp":AirTemp,"HlyDewPnt":DewPoint,"HlyEto":ETO,"HlyPrecip":Rain,"HlyRelHum":RelHumid,"HlySoilTmp":SoilTemp,"HlySolRad":SolarRad,"HlyVapPres":VaporPressure,"HlyWindDir":WindDir,"HlyWindSpd":WindSpeed})
                
        if(len(datapoints) > 0):
            db.CIMIS_Data_Cache.insert(datapoints)
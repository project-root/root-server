
import datetime
import helpers
import pandas as pd

from pymongo import MongoClient

class currentZoneValues(object):

    #default constructor is in 15 minute increment
    def __init__(self, ZoneID, Nodes, StartTime=(datetime.datetime.now() - datetime.timedelta(minutes=20)), EndTime=(datetime.datetime.now() + datetime.timedelta(minutes=5))):
        self._helper = helpers.zoneDateRange(ZoneID,StartTime,EndTime)
        self.ZoneID = self._helper.ZoneID
        self.StartTime = self._helper.StartTime
        self.EndTime = self._helper.EndTime
        self.Nodes = Nodes
        self.ZoneValues = self._getZoneData()
        
        
    def _getZoneData(self):
      
        #function to get the values from a node for a time range, will average values if there are more than one
        def getNodeData(listOfNodes):
            data = db.data
            out = {}
            noData = True
            for node in listOfNodes:
                out[node] = {"Moisture":0.0,"Air Temp":0.0,"Ground Temp":0.0,"Humidity":0.0,"i":1}
            for data in data.find({"NodeID":{"$in":listOfNodes},"Time":{"$gte":self.StartTime,"$lte":self.EndTime}}):
                #perform rolling averages for data points
                out[data["NodeID"]]["Moisture"] = (out[data["NodeID"]]["Moisture"] + data["Values"]["Moisture"])/out[data["NodeID"]]["i"]
                out[data["NodeID"]]["Air Temp"] = (out[data["NodeID"]]["Air Temp"] + data["Values"]["Air Temp"])/out[data["NodeID"]]["i"]
                out[data["NodeID"]]["Ground Temp"] = (out[data["NodeID"]]["Ground Temp"] + data["Values"]["Ground Temp"])/out[data["NodeID"]]["i"]
                out[data["NodeID"]]["Humidity"] = (out[data["NodeID"]]["Humidity"] + data["Values"]["Humidity"])/out[data["NodeID"]]["i"]
                out[data["NodeID"]]["i"] += 1
                noData = False
            if noData:
                out = {}
            return out
        
        #mongoDB connection
        client = MongoClient('mongodb://localhost')
        db = client.root
        
        #create dataframe with values
        listOfNodes = self.Nodes
        nodeValues = getNodeData(listOfNodes)
        client.close()
        
        
        
        #if there was no data returned, clear out the nodes so we dont create an empty index
        if not nodeValues:
            listOfNodes = []
            
        nodeFrame = pd.DataFrame(index=listOfNodes,columns=("Moisture","AirTemp","GroundTemp","Humidity"))

        for node,val in nodeValues.iteritems():
            nodeFrame.loc[node] = val["Moisture"],val["Air Temp"],val["Ground Temp"],val["Humidity"]
        
        badNodes = {}
        for index, row in nodeFrame.iterrows():
            badString = ""
            if row["Moisture"] == 0 and row["GroundTemp"] == 0 and row["AirTemp"] == 0 and row["Humidity"] == 0 :
                badString = "No Recent Transmissions from Node"
            if len(badString) > 0:
                badNodes[index] = badString
                nodeFrame = nodeFrame.drop(index)

        '''#run the trap
        nodeZscores = pd.DataFrame(index=listOfNodes,columns=("Moisture","AirTemp","GroundTemp","Humidity"))
        for col in list(nodeFrame.columns):
            if nodeFrame[col].std(ddof=0) == 0:
                nodeZscores[col] = 0
            else:
                nodeZscores[col] = (nodeFrame[col] - nodeFrame[col].mean())/nodeFrame[col].std(ddof=0)
        
        print nodeZscores
        #use the zscores to remove the bad nodes and put them in a dict with the reason(s) they are bad
        
        for index, row in nodeFrame.iterrows():
            
            if (row["Moisture"]) > 2:
                badString += "Abnormally High Moisture Readings  "
            elif (row["AirTemp"]) > 2:
                badString += "Abnormally High Air Temperature Readings  "
            elif (row["GroundTemp"]) > 2:
                badString += "Abnormally High Ground Temperature Readings  "
            elif (row["Humidity"]) > 2:
                badString += "Abnormally High Humidity Readings  "
            elif (row["Moisture"]) < -2:
                badString += "Abnormally Low Moisture Readings  "
            elif (row["AirTemp"]) < -2:
                badString += "Abnormally Low Air Temperature Readings  "
            elif (row["GroundTemp"]) < -2:
                badString += "Abnormally Low Ground Temperature Readings  "
            elif (row["Humidity"]) < -2:
                badString += "Abnormally Low Humidity Readings  "

            if len(badString) > 0:
                badNodes[index] = badString
                nodeFrame = nodeFrame.drop(index)'''
        
        return {"Moisture":nodeFrame["Moisture"].quantile(.3),"AirTemp":nodeFrame["AirTemp"].quantile(.3),"GroundTemp":nodeFrame["GroundTemp"].quantile(.3),"Humidity":nodeFrame["Humidity"].quantile(.3),"Good":nodeFrame,"Bad":badNodes}
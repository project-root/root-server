from __future__ import division
import time as pytime
import math
import json
import datetime
import infillCalc
import futureWeather
import soilProfile
import currentZoneValues
import noaaHelpers
import pandas as pd
from multiprocessing import Process, Manager
from pymongo import MongoClient

class analyzer(object):

    def __init__(self):
        client = MongoClient('mongodb://localhost')
        db = client.root
        
        self.DataDir = "/home/ubuntu/dev/root-server/Data/NDFD"
        
        manager = Manager()
        Zones = manager.Queue()
        Schedules = manager.Queue()
        
        for data in db.zones.find({}):
            Zones.put(data['ZoneID'])
            
        client.close()
        
        #Zones.put("1933122925544d930227b617.71505506")
        
        workers = 2
        processes = []
        
        #update weather data
        noaaHelpers.download(self.DataDir)
        
        for w in xrange(workers):
            p = Process(target=self.getSchedule, args=(Zones,Schedules,))
            p.start()
            processes.append(p)
            
        for p in processes:
            p.join()
    
    def getSchedule(self, Zones, Schedules):
        while not Zones.empty():
            ZoneID = Zones.get()
            timestart = pytime.time()
            
            #get the list of nodes from a zone
            client = MongoClient('mongodb://localhost')
            db = client.root
            data = db.zones
            Nodes = []
            for data in data.find({"ZoneID":ZoneID}):
                Nodes = data["Nodes"]
                
            if len(Nodes) != 0:
                cropName = "Almonds"
                cropVariety = "Generic"
                startDate = datetime.datetime.now()
                for data in db.zones.find({"ZoneID":ZoneID}):
                    cropName = data["Crop"]
                    cropVariety = data["Variety"]
                    if 'Plant Date' in data:
                        startDate = data["Plant Date"]
                
                rootDepth = 10
                cropMad = 50
                ETCo = 1
                StageName = "None"
                for data in db.crops.find({"Crop Name":cropName,"Crop Variety":cropVariety}):

                    if 'Stage Dates' in data:          
         
                        #Find which stage the crop currently is in
                        dates = data['Stage Dates']
                        stage = -1
                        
                        #if it is an annual crop, then we have to use the absolute date
                        if data['Annual'] == 'True':
                            for date in dates:
                                date = datetime.datetime.strptime(str(date) + " " + str(startDate.year), '%m/%d %Y')
                                
                                if startDate < date:
                                    break
                                else:
                                    stage += 1

                        #If it is not annual, then we use the relative days since planting
                        else:
                            timeDiff = datetime.datetime.now() - startDate
                            for date in dates:
                                if timeDiff.days < int(date):
                                    break
                                else:
                                    stage += 1
                        
                        #make sure stage is at least 0
                        stage = max(stage,0)
                        
                        #Get the right values for the stage
                        rootDepth = float(data['Root Depth'][stage])
                        cropMad = float(data['MAD'][stage])
                        ETCo = float(data['ET Coefficient'][stage])
                        StageName = data['Stage Names'][stage]
                
                currentValues = currentZoneValues.currentZoneValues(ZoneID,Nodes).ZoneValues #USING BOTTOM 1/3 QUANTILE FOR VALUES, NOT FILTERING VALUES CURRENTLY
                weather = futureWeather.futureWeather(ZoneID,ETCo).Weather #NEED TO GET THE CORRECT ET CROP COEFFICIENT FROM THE PLANT DB
                soil = soilProfile.soilProfile(ZoneID,Nodes,rootDepth,cropMad).Profile #NEED TO GET THE CORRECT MAD AND ROOT DEPTH FROM THE PLANT DB
                Rate = infillCalc.infillCalc(ZoneID,Nodes)
                infillRate = Rate.infillRate #CURRENTLY ASSUMING A STRAIGHT LINE SLOPE, SHOULD BE OKAY EVEN FOR LONG TERM??
                depletionRate = Rate.depletionRate #CURRENTLY ASSUMING A STRAIGHT LINE SLOPE, SHOULD BE OKAY EVEN FOR LONG TERM??
                currentMoisture = currentValues["Moisture"]
                
                error = False
                if math.isnan(currentMoisture):
                    error = True
                                        
                #calculate next irrigation time
                prevTime1 = datetime.datetime.now()
                
                #using ET (ASSUMING THAT THE UC DAVIS Available Water Storage ACTAULLY WORKS???)
                for time in sorted(weather.keys()):
                    moistureAtIrrigationET = currentMoisture*soil['Conversion Factor']
                    if 'ET1' in weather[time]:
                        moistureAtIrrigationET = moistureAtIrrigationET - weather[time]['ET1']
                        if moistureAtIrrigationET < soil['MAD']*soil['Conversion Factor']:
                            break
                        prevTime1 = time
                
                prevTime2 = datetime.datetime.now()            
                #using previous depletion rate
                if depletionRate > 0:
                    moistureAtIrrigation = currentMoisture
                    while moistureAtIrrigation > soil['MAD']:
                        prevTime2 = prevTime2 + datetime.timedelta(minutes=15)
                        moistureAtIrrigation -= depletionRate
                else:
                    error = True
                
                
                #calculate irrigation duration
                irrigationMinutes = 0
                futureMoisture = moistureAtIrrigation
                if infillRate > 0:
                    while futureMoisture < soil['FC']:
                        irrigationMinutes += 15
                        futureMoisture += infillRate
                else:
                    error = True
                
                
                
                
                '''print " " 
                print ZoneID
                print "Current Time: " + str(datetime.datetime.now())
                print "Problems: " + str(currentValues['Bad'])
                print "Current Moisture Level: " + str(currentMoisture)
                print "Current Moisture Level in cm: " + str(currentMoisture*soil['Conversion Factor'])
                print "Target Mad: " + str(soil['MAD'])
                print "Target FC: " + str(soil['FC'])
                print "Target SAT: " + str(soil['SAT'])
                print "Target PWP: " + str(soil['PWP'])
                print "Conversion Factor: " + str(soil['Conversion Factor'])
                print "Target Mad in cm: " + str(soil['MAD']*soil['Conversion Factor'])
                print "Target FC in cm: " + str(soil['FC']*soil['Conversion Factor'])
                print "Target PWP in cm: " + str(soil['PWP']*soil['Conversion Factor'])
                print "Next irrigation Time: " + str(prevTime1)
                print "Next irrigation Time: " + str(prevTime2)
                print "Moisture Level at irrigation: " + str(moistureAtIrrigation)
                print "Infill Rate: " + str(infillRate)
                print "Depletion Rate: " + str(depletionRate)
                print "Irrigate for " + str(irrigationMinutes) + " minutes"
                print " "'''
                
                
                
                
                
                #convert DataFrames To Dicts
                currentValues['Good'] = json.loads(currentValues['Good'].to_json(orient="index"))
                soil['Values'] = json.loads(soil['Values'].to_json(orient="index"))
                
                
                
                
                
                #Convert datetimes to strings
                weather2 = {}
                for time in weather:
                    weather2[str(time)] = weather[time]
                
                
                
                #Insert into DB for future use
                db.schedule.update({"ZoneID":ZoneID},{"ZoneID":ZoneID,"Current Stage":StageName,"Irrigation Time (Previous Rate)":prevTime2,"Irrigation Time (ET)":prevTime1,"Irrigation Duration":irrigationMinutes,"Depletion Rate":depletionRate,"Infill Rate":infillRate,"Current Values":currentValues,"Soil":soil,"Weather":weather2,"Error":error},upsert=True)
                
                print pytime.time() - timestart
            
            client.close()
            
a = analyzer()

from datetime import datetime
from dateutil.relativedelta import relativedelta
from pymongo import MongoClient
import random

listOfNodes = ["23456"]

client = MongoClient('mongodb://localhost')

db = client.root

for NodeID in listOfNodes:
	print NodeID
	datapoints = []
	
	time = datetime.now() + relativedelta( years = -1 )
	x = 20
	y = 58
	z = 10
	zup = True
	
	while (time < datetime.now()):
		
		month = time.month
		
		if(month == 1):
			y = 58
		elif(month == 2):
			y = 59
		elif(month == 3):
			y = 60
		elif(month == 4):
			y = 64
		elif(month == 5):
			y = 75
		elif(month == 6):
			y = 78
		elif(month == 7):
			y = 79
		elif(month == 8):
			y = 77
		elif(month == 9):
			y = 73
		elif(month == 10):
			y = 65
		elif(month == 11):
			y = 60
		elif(month == 12):
			y = 55
			
		if(x == 0):
			x = 90
		else:
			x -= 1
			
		if(zup):
			z += 1
		else:
			z -= 1
		
		if(z > 95 or z < 5):
			zup = not zup

		datapoints.append({"NodeID":NodeID,"Time":time,"Values":{"Humidity":random.randint(z,z+5),"Ground Temp":random.randint(y,y+2),"Air Temp":random.randint(y,y+7),"Moisture":random.randint(x,x+10)}})
		time = time + relativedelta( minutes = +15 )
          
	if(len(datapoints) > 0):
		db.data.insert(datapoints)

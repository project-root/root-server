from __future__ import division
import math
import datetime

class ET_Calculator(object):

    def __init__(self,t1,t2,rh,elev,windspeed,cloudcover,lat,lon,startdate,enddate,timezone_offset):
    
        if(timezone_offset == -8):
            self.median_lon = 120
        elif(timezone_offset == -7):
            self.median_lon = 105
        elif(timezone_offset == -6):
            self.median_lon = 90
        elif(timezone_offset == -5):
            self.median_lon = 75
        
        self.rh = rh
        self.SBC = 2.04E-10
        self.Gsc = 0.082
        self.specificHeat = 0.001013
        self.sigma = 0.622
        self.lam = 2.45
        self.gasConstant = 287.05
        self.t1 = ((t1 - 32) * 5.0/9.0)
        self.t2 = ((t2 - 32) * 5.0/9.0)
        self.avgtemp = (self.t1 + self.t2 ) / 2
        self.soilDepth = 0.1
        self.soilHeatCap = 2.1
        self.lat = lat
        self.lon = lon
        self.date = startdate
        self.starttime = int(startdate.time().strftime('%H%M'))
        self.endtime = int(enddate.time().strftime('%H%M'))    
        self.midtime = (self.starttime + self.endtime ) / 200
        self.windspeed = self.convertWindSpeed(windspeed)
        self.cloudcover = cloudcover
        self.elev = elev
        self.aerodynamicResistance = 208 / self.windspeed
        self.pressure = 101.3*((293-0.0065*self.elev)/293)**5.26
        self.meanAirDensity = self.pressure  / ( self.gasConstant * (self.avgtemp + 273.15) )
        self.saturationVaporPressure = self.getSaturationVaporPressure()
        self.actualVaporPressure = self.saturationVaporPressure * self.rh
        self.soilFlux = self.soilHeatCap*((self.t1-self.t2)/(1/24))*self.soilDepth        
        
        self.eprime = 0.34-(0.14*math.sqrt(self.actualVaporPressure))
        self.psychrometricConstant = ((self.specificHeat * self.pressure) / (self.sigma * self.lam))
        self.netRads = self.getNetRads()[0]
        if (self.netRads > 0):
            self.G = 0.1 * self.netRads
            self.modPsychrometricConstant = self.psychrometricConstant * (1 + (0.24*max(0.5,self.windspeed)))
        elif (self.netRads <= 0):
            self.G = 0.5 * self.netRads
            self.modPsychrometricConstant = self.psychrometricConstant * (1 + (0.96*max(0.5,self.windspeed)))
        
        self.delta = 4099 * self.saturationVaporPressure / ((self.avgtemp + 237.3)**2)
        
        self.ET_Rad = ( 0.408 * self.delta * (self.netRads - self.G) )
        
        self.ET_Aero = (self.meanAirDensity * self.specificHeat) * ((self.saturationVaporPressure - self.actualVaporPressure) / self.aerodynamicResistance)
        
        self.ET_Aero2 = (37 * self.psychrometricConstant / (self.avgtemp + 273)) * self.windspeed * (self.saturationVaporPressure - self.actualVaporPressure)
        
        self.ET = (self.ET_Rad + self.ET_Aero) / ( self.delta + self.modPsychrometricConstant)
        
        self.ET2 = (self.ET_Rad + self.ET_Aero2) / ( self.delta + self.modPsychrometricConstant)
        
        self.ET3 = 0.0023 * (self.avgtemp + 17.8) * ((max(t1,t2) - min(t1,t2))**0.5) * self.getNetRads()[1]

    def date_to_julian_day(self):
        my_date = self.date
        a = (14 - my_date.month)//12
        y = my_date.year + 4800 - a
        m = my_date.month + 12*a - 3
        return my_date.day + ((153*m + 2)//5) + 365*y + y//4 - y//100 + y//400 - 32045
    
    def convertWindSpeed(self,speed,meters=10.0):
        return speed * (4.87 / (math.log(67.8*meters - 5.42)))
        
    def getSaturationVaporPressure(self):
        eso=6.1078
        t = self.avgtemp
        c0=0.99999683
        c1=-0.90826951E-02
        c2=0.78736169E-04
        c3=-0.61117958E-06
        c4=0.43884187E-08
        c5=-0.29883885E-10
        c6=0.21874425E-12
        c7=-0.17892321E-14
        c8=0.11112018E-16
        c9=-0.30994571E-19
        
        pol=c0+t*(c1+t*(c2+t*(c3+t*(c4+t*(c5+t*(c6+t*(c7+t*(c8+t*(c9)))))))))
        
        es=eso*math.pow(pol,-8)

        return es

    def getNetRads(self):
    
        #Radian Calculation
        rad=((math.pi)/180)*self.lat
        J = self.date_to_julian_day()
        
        #Inverse Relative Distance from the Earth To Sun
        drcos=(2*(math.pi)*J)/365
        dr=1+(0.033*math.cos(drcos))
        
        
        #Solar Declination C-1
        decsin=((2*(math.pi)*J)/365)-1.39
        dec=0.409*math.sin(decsin)
        
        
        #solar time angle at midpoint of period
        Scfunc=2*math.pi*(J-81)/364
        Sc=(0.1645*math.sin(2*Scfunc))-(0.1255*math.cos(Scfunc))-(0.025*math.sin(Scfunc))
        
        #hour angle in radians
        w=(math.pi/12)* ((self.midtime-0.5)+((self.median_lon-self.lon)/15)-12+Sc)
        
        #Solar Time at beginning and end of a period
        w1=w-(math.pi/24)
        w2=w+(math.pi/24)
        
        
        #Ra Calculation
        Rebrac=(w2-w1)*math.sin(rad)*math.sin(dec)+math.cos(rad)*math.cos(dec)*(math.sin(w2)-math.sin(w1))
        Ra=12*60*self.Gsc*dr*Rebrac/math.pi
        
        #Solar altitude in degrees
        beta = (180 / math.pi) * math.asin( (math.sin(rad) * math.sin(dec))  +  (math.cos(rad) * math.cos(dec) * math.cos(w)))
        
        #Clear Sky Solar Radiation
        Rso=(0.75+(self.elev*2E-5))*Ra
        
        #Solar Radiation
        Rs=(0.25+(0.50*self.cloudcover))*Ra
        
        f=((1.35*(Rs/Rso))-0.35)
        
        #Net Solar Shortwave Radiation
        Rns=(1-0.23)*Rs
        
        Rnl= -1 * f * self.eprime * self.SBC * ((self.avgtemp + 273.15)**4)
        
        #Net Radiation
        Rn=Rns-Rnl
        
        return [Rn,Ra]
       
        

    '''
    ET			Evapotranspiration [mm day-1]
    delta		Slope of the saturation vapour pressure temperature relationship curve [kPa C-1]
    Rn	 		The net radiation [MJ m-2 day-1]
    Re			Extraterrestrial Radiation [MJ m-2 day-1]
    Rs			Solar Radiation[MJ m-2 day-1]
    Rso			Clear-Sky Solor Radiation [MJ m-2 day-1]
    alpha		albedo or canopy reflection coefficient(typically 0.05 to 0.95)
    G   		Soil heat flux density [MJ m-2 day-1]
    Pa  		Mean air density at constant pressure [kg m-3]
    Cp  		Specific heat of the air at a constand pressure [MJ kg-1 C-1]
    es  		Saturation vapour pressure [kPa]
    ea  		Actual vapour pressure [kPa]
    es - ea		Saturation vapour pressure deficit [kPa]
    P			Atmospheric Pressure [kPa]
    z			self.elevation above sea level [m]
    lam			Latent heat of vaporization [MJ kg-1]
    E			ratio molecular weight of water vapor/dry air
    y 			Psychrometric constant [kPa C-1]
    ra 			Aerodynamic resistance [s m-1],
    zm 			Height of wind measurements [m]
    zh 			Height of humidity measurements [m]
    d 			Zero plane displacement height [m]
    Ch          Canopy Height
    zom			Roughness length governing momentum transfer [m]
    zoh 		Roughness length governing transfer of heat and vapour [m]
    k 			Von Karman's constant  [-]
    uz 			Wind speed at height z [m s-1]
    rs 			(bulk) surface resistance [s m-1]
    rl 			Bulk stomatal resistance of the well-illuminated leaf [s m-1],
    LAIactive 	Active (sunlit) leaf area index [m2 (leaf area) m-2 (soil surface)]
    T			Air Temperature[degrees C]
    Tmin		Minimum Air Temperature[degrees C]
    Tmax		Maximum Air Temperature[degrees C]
    dr			Inverse relative distance of Earth-Sun
    dec			Solar Declination
    ws			Sunset hour angle[Rad]
    J			Day of the year [1-365or366]
    Gsc			Solar Constant [MJ m-2 min-1]
    rad			laditude
    Ldeg		latitude degrees
    Lmin		latitude minutes
    N			Daylight hours [hours day-1]
    n			Actual hours of sunshine[hours day-1]
    As			Regression constant, extraterrestrial radiation reaching the earth on overcast days (n = 0)
    As+bs		Fraction of extraterrestrial radiation reaching the earth on clear days (n = N)
    SBC			Stephen-Boltzmann Constant[MJ K-4 m-2 day-1]
    


    a = ET_Calculator(t1=85,t2=87,rh=.4,elev=10,windspeed=5,cloudcover=.6,lat=37.555525,lon=-122.053122,startdate=datetime.datetime.now(),enddate=(datetime.datetime.now() + datetime.timedelta(hours=1)),timezone_offset = -8)
    print a.ET
    print a.ET2
    print a.crapET
    
    '''